import * as config from './default.json';
import * as path from "path";

const algoDir = path.dirname(require.main.filename) + "/Algo/";

interface IConfig {
    dbConfig: {
        uri: string;
        dbName: string;
    },
    algo: {
        pythonLocation: string;

        roommatesRecommendationAlgoScriptFile: string;
        allRoommatesChoicesGroupedByEmailsFileName: string;
        allApartmentsUsersAsVectorFileName: string;
        roommatesRecommendationAlgoResultsFileName: string;
        roommatesLabelsAlgoResultsFileName: string;

        apartmentsRecommendationAlgoScriptFile: string;
        allApartmentsChoicesGroupedByEmailsFileName: string;
        allRoommatesUsersAsVectorFileName: string;
        apartmentsRecommendationAlgoResultsFileName: string;
        apartmentsLabelsAlgoResultsFileName: string;
    }

}

let configInstance: IConfig = {
    dbConfig: {
        uri: (<any>config).dbConfig.uri,
        dbName: (<any>config).dbConfig.dbName
    },
    algo: {
        pythonLocation: '/usr/bin/python3.6',

        roommatesRecommendationAlgoScriptFile: algoDir + "roommatesRecommendationAlgoScriptFile.py",
        allRoommatesChoicesGroupedByEmailsFileName: algoDir + "allRoommatesChoicesGroupedByEmailsFileName.json",
        allApartmentsUsersAsVectorFileName: algoDir + "allApartmentsUsersAsVectorFileName.json",
        roommatesRecommendationAlgoResultsFileName: algoDir + "roommatesRecommendationAlgoResultsFileName.json",
        roommatesLabelsAlgoResultsFileName: algoDir + "roommatesLabelsAlgoResultsFileName.json",

        apartmentsRecommendationAlgoScriptFile: algoDir + "apartmentsRecommendationAlgoScriptFile.py",
        allApartmentsChoicesGroupedByEmailsFileName: algoDir + "allApartmentsChoicesGroupedByEmailsFileName.json",
        allRoommatesUsersAsVectorFileName: algoDir + "allRoommatesUsersAsVectorFileName.json",
        apartmentsRecommendationAlgoResultsFileName: algoDir + "apartmentsRecommendationAlgoResultsFileName.json",
        apartmentsLabelsAlgoResultsFileName: algoDir + "apartmentsLabelsAlgoResultsFileName.json",
    }
};

export default configInstance;

