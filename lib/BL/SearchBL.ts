import {SearchDal} from "../DAL/SearchDal";
import UserChoicesBL from "./UserChoicesBL";
import AlgoBl from "./AlgoBl";
import {UserTypeEnum} from "../shared/Enums/UserTypeEnum";
import UserBl from "./UserBl";
import {EmailWithCounter} from "../shared/Models/EmailWithCounter";
import UserModel from "../shared/Models/UserModel";

export class SearchBL {
    private _searchDal: SearchDal;
    private _userChoicesBl: UserChoicesBL;
    private _algoBl: AlgoBl;
    private _userBl: UserBl;

    constructor() {
        this._searchDal = new SearchDal();
        this._userChoicesBl = new UserChoicesBL();
        this._userBl = new UserBl();
        this._algoBl = new AlgoBl();
    }

    public async getUserSuitableSearchResults(userEmail: string, maxResults: number): Promise<Array<UserModel>> {
        let userLabel: number;
        let zeroLabel: number;

        let emailsWithCounter: Array<EmailWithCounter> = null;
        let {emailsAfterFilters, userType} = await this._searchDal.getSearchResultsByUserPreferences(userEmail);

        if (userType === UserTypeEnum.APARTMENT_SEARCH) {
            userLabel = await this._algoBl.getRoommateLabelByEmail(userEmail);
            zeroLabel = await this._algoBl.getRoommateZeroLabel();
            emailsWithCounter =
                await this._algoBl.getRoommateRecommendationsByLabel(userLabel, emailsAfterFilters);
        } else {
            userLabel = await this._algoBl.getApartmentLabelByEmail(userEmail);
            zeroLabel = await this._algoBl.getApartmentZeroLabel();
            emailsWithCounter =
                await this._algoBl.getApartmentRecommendationsByLabel(userLabel, emailsAfterFilters);
        }

        if ((emailsAfterFilters.length > emailsWithCounter.length) &&
            (maxResults > emailsWithCounter.length)) {
            return await this._userBl.getUsersByEmails(emailsAfterFilters.splice(0, maxResults));
        }

        emailsWithCounter.sort((first, second) => {
            if (zeroLabel === userLabel) {
                if (first.counter === 0) return -1; // make zero first
                else if (second.counter === 0) return 1; // make zero first
                else if (first.counter > second.counter) return 1; // make lower first
                else return -1;
            } else {
                if (first.counter > second.counter) return -1;
                else return 1;
            }
        });

        let idsOfProfilesWeRecommend = emailsWithCounter.map((currEmailWithCounter) => {
            return currEmailWithCounter.email;
        });

        if (emailsWithCounter.length > maxResults) {
            idsOfProfilesWeRecommend = idsOfProfilesWeRecommend.splice(0, maxResults);
        }

        return await this._userBl.getUsersByEmails(idsOfProfilesWeRecommend);
    }
}