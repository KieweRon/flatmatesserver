import UserChoicesDal from "../DAL/UserChoicesDal";
import UserChoice from "../shared/Models/UserChoiceFromDb";
import UserDal from "../DAL/UserDal";
import User from "../shared/Models/UserModel";

export class UserChoicesBL {

    UserChoicesDAL: UserChoicesDal = new UserChoicesDal();
    userDAL: UserDal = new UserDal();

    public async saveNewChoice(choiceToSave: UserChoice) {
        return await this.UserChoicesDAL.saveNewChoice(choiceToSave);
    }

    public async saveNewChoices(userChoices: Array<UserChoice>) {
        return await this.UserChoicesDAL.saveNewChoices(userChoices);
    }

    public async deleteUserChoice(choiceToDelete: UserChoice) {
        return await this.UserChoicesDAL.deleteUserChoice(choiceToDelete);
    }

    public async deleteAllUserChoices(userEmail: string) {
        return await this.UserChoicesDAL.deleteAllUserChoices(userEmail);
    }

    public async deleteAllUserDislikes(userEmail: string) {
        return await this.UserChoicesDAL.deleteAllUserDislikes(userEmail);
    }

    public async getLikedUsersByLikingUserEmail(likingUserEmail: string): Promise<any> {
        return await this.UserChoicesDAL.getLikedUsersByLikingUserEmail(likingUserEmail);
    }

    public async getLikingUsersByLikedUserEmail(likedUserEmail: string) {
        return await this.UserChoicesDAL.getLikingUsersByLikedUserEmail(likedUserEmail);
    }

    public async checkIfMatch(firstUserEmail: string, secondUserEmail: string): Promise<boolean> {
        return await this.UserChoicesDAL.checkIfMatch(firstUserEmail, secondUserEmail);
    }

    public async getUserMatcheChoices(userEmail: string): Promise<Array<UserChoice>> {
        var likesArray = await this.UserChoicesDAL.getLikesByLikingUserEmail(userEmail);
        var myMatches: Array<UserChoice> = new Array<UserChoice>();

        for (let i = 0; i < likesArray.length; i++) {
            if (await this.UserChoicesDAL.checkIfMatch(userEmail, likesArray[i].secondUser)) {
                let currMatch = await this.UserChoicesDAL.getLikeByUserEmails(likesArray[i].secondUser, userEmail);
                
                if (currMatch.timestamp < likesArray[i].timestamp) {
                    currMatch.timestamp = likesArray[i].timestamp;
                }

                myMatches.push(currMatch);
            }
        }
        return myMatches;
    }

    public async getUserMatchesByLastPollDate(userEmail: string, lastPollDate: Date) {
        let matches = await this.getUserMatcheChoices(userEmail);
        let userMatchesToNotify: Array<User> = new Array<User>();

        //TODO: promise all
        for (let i = 0; i < matches.length; i++) {
            if (matches[i].timestamp > new Date(lastPollDate)) {
                let currUserEmail = matches[i].firstUser;

                if (matches[i].secondUser !== userEmail) {
                    currUserEmail = matches[i].secondUser;
                }

                await this.userDAL.getUserByEmail(currUserEmail).then(user => userMatchesToNotify.push(user));
            }
        }

        return userMatchesToNotify;
    }

    public async getUserMatches(userEmail: string): Promise<User[]> {
        let likesArray = await this.UserChoicesDAL.getLikesByLikingUserEmail(userEmail);

        let allEmailsILiked = likesArray.map((currChoice) => {
           return currChoice.secondUser;
        });

        let allEmailsWithMatch = await this.UserChoicesDAL.getAllEmailsThatLikedMeFromList(allEmailsILiked, userEmail);

        let allUsersWithMatch = await this.userDAL.getUsersByEmails(allEmailsWithMatch);

        return allUsersWithMatch.filter((currUser) => {
            return (!currUser.isDeleted && currUser.email != userEmail);
        });
    }

    public async getTopTenLikedAppartments() {
        return await this.UserChoicesDAL.getTopTenLikedAppartments();
    }

    public async getLikedApartmentProfilesByLikingUserEmail(likingUserEmail: string) {
        return await this.UserChoicesDAL.getLikedApartmentProfilesByLikingUserEmail(likingUserEmail);
    }

    public async getLikedRoomateProfilesByLikingUserEmail(likingUserEmail: string) {
        return await this.UserChoicesDAL.getLikedRoomateProfilesByLikingUserEmail(likingUserEmail);
    }

    public async getAllRoommatesUserChoicesGroupedByEmails() {
        return await this.UserChoicesDAL.getAllRoommatesUserChoicesGroupedByEmails();
    }

    public async getAllApartmentsUserChoicesGroupedByEmails() {
        return await this.UserChoicesDAL.getAllApartmentsUserChoicesGroupedByEmails();
    }
}

export default UserChoicesBL;