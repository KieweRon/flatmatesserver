import User from "../shared/Models/UserModel";
import ApartmentAsVector from "../shared/Models/ApartmentAsVector";
import {UserChoiceGroupedByEmail} from "../shared/Models/UserChoiseGroupedByEmail";
import {PythonShell} from 'python-shell';
import * as fs from "fs";
import UserBl from "./UserBl";
import UserChoicesBL from "./UserChoicesBL";
import config from '../../config/config';
import AlgoRecommendationResults from "../shared/Models/AlgoRecommendationResults";
import AlgoDal from "../DAL/AlgoDal";
import RoommatesAsVector from "../shared/Models/RoommatesAsVector";
import AlgoLabelingResults from "../shared/Models/AlgoLabelingResults";
import {EmailWithCounter} from "../shared/Models/EmailWithCounter";
import {ChoiceTypeEnum} from "../shared/Enums/ChoiceTypeEnum";
import {UserTypeEnum} from "../shared/Enums/UserTypeEnum";

export class AlgoBl {
    _userChoicesBL: UserChoicesBL;
    _algoDal: AlgoDal;
    _userBL: UserBl;

    constructor() {
        this._userChoicesBL = new UserChoicesBL();
        this._algoDal = new AlgoDal();
        this._userBL = new UserBl();
    }

    // חישוב ההמלצות למשתמשים מסוג מחפשי דירה
    public async calcRecommendationsForRoommatesProfile() {
        return new Promise(async (resolve, reject) => {
            try {
                let allUserChoicesGroupedByEmails: Array<UserChoiceGroupedByEmail> =
                    await this._userChoicesBL.getAllRoommatesUserChoicesGroupedByEmails();

                let allUsersWithChoices = allUserChoicesGroupedByEmails.map((currUser) => {
                    return currUser._id
                });

                let allUserWithoutChoices: Array<Partial<User>> =
                    await this._userBL.getAllEmailsIDidntGetByType(allUsersWithChoices, UserTypeEnum.APARTMENT_SEARCH);

                allUserChoicesGroupedByEmails = allUserWithoutChoices.reduce((previousValue, currentValue) => {
                    previousValue.push({
                        _id: currentValue.email,
                        profilesIChose: []
                    });

                    return previousValue;
                }, allUserChoicesGroupedByEmails);

                let allApartmentsUsers: Array<Partial<User>> = await this._userBL.getAllApartmentSearchUsersForAlgo();

                let allApartmentsUsersAsVector = allApartmentsUsers.map((currProfile) => {
                    let currVector = (new ApartmentAsVector([currProfile])).finalVector;
                    currVector.email = currProfile.email;

                    return currVector;
                });

                fs.writeFileSync(config.algo.allRoommatesChoicesGroupedByEmailsFileName,
                    JSON.stringify(allUserChoicesGroupedByEmails));
                fs.writeFileSync(config.algo.allApartmentsUsersAsVectorFileName,
                    JSON.stringify(allApartmentsUsersAsVector));

                let options = {
                    pythonPath: config.algo.pythonLocation,
                    args: [
                        config.algo.allRoommatesChoicesGroupedByEmailsFileName,
                        config.algo.allApartmentsUsersAsVectorFileName,
                        config.algo.roommatesRecommendationAlgoResultsFileName,
                        config.algo.roommatesLabelsAlgoResultsFileName
                    ]
                };

                return await PythonShell.run(config.algo.roommatesRecommendationAlgoScriptFile, options,
                    async (err, pythonResults) => {
                        if (err) return reject(err);

                        let recommendationsForRoommates: Array<AlgoRecommendationResults> =
                            JSON.parse(fs.readFileSync(pythonResults[0], 'utf8'));
                        let roommatesWithNewLabels: Array<AlgoLabelingResults> =
                            JSON.parse(fs.readFileSync(pythonResults[1], 'utf8'));

                        await this.saveRecommendationsForRoommatesProfilesInDB(recommendationsForRoommates);

                        let saveRoommatesLabelsResult =
                            await this.saveRoommatesLabelsInDB(roommatesWithNewLabels);

                        resolve(saveRoommatesLabelsResult);
                    });
            } catch (err) {
                throw err;
            }
        });
    }

    public async calcRecommendationsForApartmentsProfile() {
        return new Promise(async (resolve, reject) => {
            try {
                let allUserChoicesGroupedByEmails: Array<UserChoiceGroupedByEmail> =
                    await this._userChoicesBL.getAllApartmentsUserChoicesGroupedByEmails();

                let allUsersWithChoices = allUserChoicesGroupedByEmails.map((currUser) => {
                    return currUser._id
                });

                let allUserWithoutChoices: Array<Partial<User>> =
                    await this._userBL.getAllEmailsIDidntGetByType(allUsersWithChoices, UserTypeEnum.ROOMMATE_SEARCH);

                allUserChoicesGroupedByEmails = allUserWithoutChoices.reduce((previousValue, currentValue) => {
                    previousValue.push({
                        _id: currentValue.email,
                        profilesIChose: []
                    });

                    return previousValue;
                }, allUserChoicesGroupedByEmails);

                let allRoommatesUsers: Array<Partial<User>> = await this._userBL.getAllRoommatesSearchUsersForAlgo();

                let allRoommatesUsersAsVector = allRoommatesUsers.map((currProfile) => {
                    let currVector = (new RoommatesAsVector([currProfile])).finalVector;
                    currVector.email = currProfile.email;

                    return currVector;
                });

                fs.writeFileSync(config.algo.allApartmentsChoicesGroupedByEmailsFileName,
                    JSON.stringify(allUserChoicesGroupedByEmails));
                fs.writeFileSync(config.algo.allRoommatesUsersAsVectorFileName,
                    JSON.stringify(allRoommatesUsersAsVector));

                let options = {
                    pythonPath: config.algo.pythonLocation,
                    args: [
                        config.algo.allApartmentsChoicesGroupedByEmailsFileName,
                        config.algo.allRoommatesUsersAsVectorFileName,
                        config.algo.apartmentsRecommendationAlgoResultsFileName,
                        config.algo.apartmentsLabelsAlgoResultsFileName
                    ]
                };

                await PythonShell.run(config.algo.apartmentsRecommendationAlgoScriptFile, options,
                    async (err, pythonResults) => {
                        if (err) return reject(err);

                        let recommendationsForApartments: Array<AlgoRecommendationResults> =
                            JSON.parse(fs.readFileSync(pythonResults[0], 'utf8'));
                        let apartmentsWithNewLabels: Array<AlgoLabelingResults> =
                            JSON.parse(fs.readFileSync(pythonResults[1], 'utf8'));

                        await this.saveRecommendationsForApartmentsProfilesInDB(recommendationsForApartments);

                        let saveApartmentsLabelsResult =
                            await this.saveApartmentsLabelsInDB(apartmentsWithNewLabels);

                        resolve(saveApartmentsLabelsResult);
                    });
            } catch (err) {
                throw err;
            }
        });
    }

    public async saveRecommendationsForRoommatesProfilesInDB(algoResults: Array<AlgoRecommendationResults>):
        Promise<number> {
        return await this._algoDal.saveRecommendationsForRoommatesProfilesInDB(algoResults);
    }

    public async saveRoommatesLabelsInDB(algoResults: Array<AlgoLabelingResults>): Promise<number> {
        return await this._algoDal.saveRoommatesLabelsInDB(algoResults);
    }

    public async saveRecommendationsForApartmentsProfilesInDB(algoResults: Array<AlgoRecommendationResults>):
        Promise<number> {
        return await this._algoDal.saveRecommendationsForApartmentsProfilesInDB(algoResults);
    }

    public async saveApartmentsLabelsInDB(algoResults: Array<AlgoLabelingResults>): Promise<number> {
        return await this._algoDal.saveApartmentsLabelsInDB(algoResults);
    }

    public async getRoommateLabelByEmail(email: string): Promise<number> {
        return await this._algoDal.getRoommateLabelByEmail(email);
    }

    public async getRoommateZeroLabel(): Promise<number> {
        return await this._algoDal.getRoommateZeroLabel();
    }

    public async getApartmentLabelByEmail(email: string): Promise<number> {
        return await this._algoDal.getApartmentLabelByEmail(email);
    }

    public async getApartmentZeroLabel(): Promise<number> {
        return await this._algoDal.getApartmentZeroLabel();
    }

    public async getRoommateRecommendationsByLabel(label: number,
                                                   emailsAfterFilters: Array<string>):
        Promise<Array<EmailWithCounter>> {
        return await this._algoDal.getRoommateRecommendationsByLabel(label, emailsAfterFilters);
    }

    public async getApartmentRecommendationsByLabel(label: number,
                                                    emailsAfterFilters: Array<string>):
        Promise<Array<EmailWithCounter>> {
        return await this._algoDal.getApartmentRecommendationsByLabel(label, emailsAfterFilters);
    }

    public async addNewRoommatesToLabels(email: string): Promise<number> {
        return await this._algoDal.addNewRoommatesToLabels(email);
    }

    public async addNewApartmentToLabels(email: string): Promise<number> {
        return await this._algoDal.addNewApartmentToLabels(email);
    }

    public async checkRecommendationsForRoommatesProfile() {
        let allUserChoicesGroupedByEmails: Array<UserChoiceGroupedByEmail> =
            await this._userChoicesBL.getAllRoommatesUserChoicesGroupedByEmails();

        let allApartmentsUsers: Array<Partial<User>> = await this._userBL.getAllApartmentSearchUsersForAlgo();

        let allApartmentsUsersAsVectorDict = allApartmentsUsers.reduce((lastObject, currProfile) => {
            let currVector = (new ApartmentAsVector([currProfile])).finalVector;
            lastObject[currProfile.email] = currVector;

            return lastObject;
        }, {});

        let zeroLabel = await this.getRoommateZeroLabel();
        let roommatesAsFinalVector = {};

        for (const currUser of allUserChoicesGroupedByEmails) {
            let currUserLabel = await this.getRoommateLabelByEmail(currUser._id);

            if (currUserLabel === zeroLabel) continue;

            let currRoommateAsFinalVectors = null;
            let likesCounter = 0;

            for (const currChose of currUser.profilesIChose) {
                if ((currChose.choiceType === ChoiceTypeEnum.DISLIKE_ROOMMATE) ||
                    (currChose.choiceType === ChoiceTypeEnum.DISLIKE_APARTMENT)) {
                    continue;
                }

                likesCounter++;

                if (currRoommateAsFinalVectors == null) {
                    currRoommateAsFinalVectors = {
                        ...allApartmentsUsersAsVectorDict[currChose.profile]
                    }
                } else {

                    Object.keys(allApartmentsUsersAsVectorDict[currChose.profile]).forEach((currKey) => {
                        currRoommateAsFinalVectors[currKey] +=
                            allApartmentsUsersAsVectorDict[currChose.profile][currKey];
                    });

                }
            }

            if (currRoommateAsFinalVectors != null) {
                currRoommateAsFinalVectors.counter = likesCounter;
                currRoommateAsFinalVectors.label = currUserLabel;
                roommatesAsFinalVector[currUser._id] = currRoommateAsFinalVectors;
            }
        }

        for (const currEmail in roommatesAsFinalVector) {
            let objectSum = 0;

            let likesCounter = roommatesAsFinalVector[currEmail].counter;

            Object.keys(roommatesAsFinalVector[currEmail]).forEach((currKey) => {
                if ((currKey === "counter") || (currKey === "label")) return;

                roommatesAsFinalVector[currEmail][currKey] /= likesCounter;
                objectSum += roommatesAsFinalVector[currEmail][currKey];
            });

            if (objectSum === 0) continue;

            Object.keys(roommatesAsFinalVector[currEmail]).forEach((currKey) => {
                if (currKey === "label") return;

                roommatesAsFinalVector[currEmail][currKey] /= objectSum;
            });

            delete roommatesAsFinalVector[currEmail].counter;
        }

        let ablest3Counter = 0;

        for (const currEmail in roommatesAsFinalVector) {
            let currLabel = roommatesAsFinalVector[currEmail].label;
            const maxResults = 10;

            delete roommatesAsFinalVector[currEmail].label;

            let emailsWithCounter = await this.getRoommateRecommendationsByLabel(currLabel, []);

            emailsWithCounter.sort((first, second) => {
                if (first.counter > second.counter) return -1;
                else return 1;
            });

            let idsOfProfilesWeRecommend = emailsWithCounter.map((currEmailWithCounter) => {
                return currEmailWithCounter.email;
            });

            idsOfProfilesWeRecommend = idsOfProfilesWeRecommend.splice(0, maxResults);

            let searchResult = await this._userBL.getUsersByEmails(idsOfProfilesWeRecommend);

            let counterMoreThan60 = 0;

            searchResult.forEach((currUser) => {
                let sum = 0;

                let currVector = allApartmentsUsersAsVectorDict[currUser.email];

                Object.keys(roommatesAsFinalVector[currEmail]).forEach((currKey) => {
                    sum += roommatesAsFinalVector[currEmail][currKey] * (currVector[currKey] as number);
                });

                if (sum > 0.50) {
                    counterMoreThan60++;
                }
            });

            if (counterMoreThan60 >= 5) {
                ablest3Counter++
            }
        }
    }

    public async checkRecommendationsForApartmentsProfile() {
        let allUserChoicesGroupedByEmails: Array<UserChoiceGroupedByEmail> =
            await this._userChoicesBL.getAllApartmentsUserChoicesGroupedByEmails();

        let allRoommatesUsers: Array<Partial<User>> = await this._userBL.getAllRoommatesSearchUsersForAlgo();

        let allRoommatesUsersAsVectorDict = allRoommatesUsers.reduce((lastObject, currProfile) => {
            let currVector = (new RoommatesAsVector([currProfile])).finalVector;
            lastObject[currProfile.email] = currVector;

            return lastObject;
        }, {});

        let zeroLabel = await this.getApartmentZeroLabel();
        let apartmentsAsFinalVector = {};

        for (const currUser of allUserChoicesGroupedByEmails) {
            let currUserLabel = await this.getApartmentLabelByEmail(currUser._id);

            if (currUserLabel === zeroLabel) continue;

            let currApartmentsAsFinalVectors = null;
            let likesCounter = 0;

            for (const currChose of currUser.profilesIChose) {
                if ((currChose.choiceType === ChoiceTypeEnum.DISLIKE_ROOMMATE) ||
                    (currChose.choiceType === ChoiceTypeEnum.DISLIKE_APARTMENT)) {
                    continue;
                }

                likesCounter++;

                if (currApartmentsAsFinalVectors == null) {
                    currApartmentsAsFinalVectors = {
                        ...allRoommatesUsersAsVectorDict[currChose.profile]
                    }
                } else {

                    Object.keys(allRoommatesUsersAsVectorDict[currChose.profile]).forEach((currKey) => {
                        currApartmentsAsFinalVectors[currKey] +=
                            allRoommatesUsersAsVectorDict[currChose.profile][currKey];
                    });

                }
            }

            if (currApartmentsAsFinalVectors != null) {
                currApartmentsAsFinalVectors.counter = likesCounter;
                currApartmentsAsFinalVectors.label = currUserLabel;
                apartmentsAsFinalVector[currUser._id] = currApartmentsAsFinalVectors;
            }
        }

        for (const currEmail in apartmentsAsFinalVector) {
            let objectSum = 0;

            let likesCounter = apartmentsAsFinalVector[currEmail].counter;

            Object.keys(apartmentsAsFinalVector[currEmail]).forEach((currKey) => {
                if ((currKey === "counter") || (currKey === "label")) return;

                apartmentsAsFinalVector[currEmail][currKey] /= likesCounter;
                objectSum += apartmentsAsFinalVector[currEmail][currKey];
            });

            if (objectSum === 0) continue;

            Object.keys(apartmentsAsFinalVector[currEmail]).forEach((currKey) => {
                if (currKey === "label") return;

                apartmentsAsFinalVector[currEmail][currKey] /= objectSum;
            });

            delete apartmentsAsFinalVector[currEmail].counter;
        }

        let ablest3Counter = 0;

        for (const currEmail in apartmentsAsFinalVector) {
            let currLabel = apartmentsAsFinalVector[currEmail].label;
            const maxResults = 10;

            delete apartmentsAsFinalVector[currEmail].label;

            let emailsWithCounter = await this.getApartmentRecommendationsByLabel(currLabel, []);

            emailsWithCounter.sort((first, second) => {
                if (first.counter > second.counter) return -1;
                else return 1;
            });

            let idsOfProfilesWeRecommend = emailsWithCounter.map((currEmailWithCounter) => {
                return currEmailWithCounter.email;
            });

            idsOfProfilesWeRecommend = idsOfProfilesWeRecommend.splice(0, maxResults);

            let searchResult = await this._userBL.getUsersByEmails(idsOfProfilesWeRecommend);

            let counterMoreThan60 = 0;

            searchResult.forEach((currUser) => {
                let sum = 0;

                let currVector = allRoommatesUsersAsVectorDict[currUser.email];

                Object.keys(apartmentsAsFinalVector[currEmail]).forEach((currKey) => {
                    sum += apartmentsAsFinalVector[currEmail][currKey] * (currVector[currKey] as number);
                });

                if (sum > 0.50) {
                    counterMoreThan60++;
                }
            });

            if (counterMoreThan60 >= 7) {
                ablest3Counter++
            }
        }
    }
}

export default AlgoBl;