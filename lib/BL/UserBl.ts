import UserDal from "../DAL/UserDal";
import UserModel from "../shared/Models/UserModel";
import User from "../shared/Models/UserModel";
import {UserTypeEnum} from "../shared/Enums/UserTypeEnum";
import {UserPreferences} from "../shared/types/UserPreferencesType";
import UserChoicesDal from "../DAL/UserChoicesDal";
import Apartment from "shared/Models/ApartmentModel";
import Roommate from "shared/Models/RoommateModel";
import AlgoBl from "./AlgoBl";

export class UserBl {

    private _userDal: UserDal = new UserDal();
    private _userChoicesDal: UserChoicesDal = new UserChoicesDal();

    constructor() {
        this._userDal = new UserDal();
        this._userChoicesDal;
    }

    public async getUserByUserEmail(userEmail: string) {
        return (await this._userDal.getUserByEmail(userEmail));
    }

    public async getUsersByEmails(userEmail: Array<string>): Promise<Array<UserModel>> {
        return (await this._userDal.getUsersByEmails(userEmail));
    }

    public async getApartmentCardPreviewDetailsByUserEmail(userEmail: string) {
        return (await this._userDal.getApartmentCardPreviewDetailsByUserEmail(userEmail));
    }

    public async signOut(email: string) {
        return await this._userDal.signOut(email);
    }

    public async registerUser(user: User) {
        const _algoBl: AlgoBl = new AlgoBl();

        let result = await this._userDal.saveNewUser(user);

        if (user.type === UserTypeEnum.APARTMENT_SEARCH) {
            await _algoBl.addNewRoommatesToLabels(user.email);
        } else {
            await _algoBl.addNewApartmentToLabels(user.email);
        }
        
        return result;
    }    

    public async updateUser(user: User): Promise<number> {
        return await this._userDal.updateUser(user);
    }
    public async updateUsersPictures() {
        return await this._userDal.updateUsersPictures();
    }

    public async signIn(email: string, password: string): Promise<UserModel> {
        var user: User = await this._userDal.signIn(email, password);
        if (!user) {
            throw new Error("incorrect email or password");
        } else {
            return user;
        }
    }

    public async switchAccountState(email: string, isDeleted: boolean) {
        return await this._userDal.switchAccountState(email, isDeleted);
    }

    public async logOut(userEmail: string) {
        let currUser: User = await this._userDal.getUserByEmail(userEmail);
        if (currUser) {
            currUser.isActive = false;
            return await this._userDal.updateUser(currUser);
        }
    }

    public async saveUserPreferences(userEmail: string, userPreferences: UserPreferences) {
        return await this._userDal.saveUserPreferences(userEmail, userPreferences);
    }

    public async changeExistingUserProfileType(userEmail: string, userNewType: string, newProfile: Apartment | Roommate, userPreferences: UserPreferences) {
        let setQueryObj: any;
        let _algoBl :AlgoBl = new AlgoBl();
      

        switch (userNewType) {
            case UserTypeEnum.ROOMMATE_SEARCH: {
            await _algoBl.addNewApartmentToLabels(userEmail);

                newProfile.picture = this._userDal.saveUserPictureToFileSystem(userEmail, newProfile.picture);
                
                for (let index = 0; index < (newProfile as Apartment).roommates.length; index++) {
                    (newProfile as Apartment).roommates[index].picture = this._userDal.saveUserPictureToFileSystem(userEmail, (newProfile as Apartment).roommates[index].picture, index + 1);
                }

                setQueryObj = {
                    $set: {
                        roommateProfile: null,
                        userPreferences: userPreferences,
                        apartmentProfile: newProfile,
                        type: "RoommateSearch"
                    }
                };
                break;
            }
            case UserTypeEnum.APARTMENT_SEARCH: {
                newProfile.picture = this._userDal.saveUserPictureToFileSystem(userEmail, newProfile.picture);
                await _algoBl.addNewRoommatesToLabels(userEmail);

                setQueryObj = {
                    $set: {
                        apartmentProfile: null,
                        userPreferences: userPreferences,
                        roommateProfile: newProfile,
                        type: "ApartmentSearch"
                    }
                };
                break;
            }
        }

        if (await this._userDal.updateUserBySetQueryObj(userEmail, setQueryObj)) {
            return await this._userChoicesDal.deleteAllUserChoices(userEmail);
        }
    }

    public async getAllApartmentSearchUsersForAlgo() {
        return await this._userDal.getAllApartmentSearchUsersForAlgo();
    }

    public async getAllRoommatesSearchUsersForAlgo() {
        return await this._userDal.getAllRoommatesSearchUsersForAlgo();
    }

    public async getAllUserTypeAndPreferencesByType(userType: UserTypeEnum): Promise<Array<Partial<User>>> {
        return await this._userDal.getAllUserTypeAndPreferencesByType(userType);
    }

    public async getAllEmailsIDidntGetByType(emailsToExclude: Array<string>, profileType: UserTypeEnum):
        Promise<Array<Partial<UserModel>>> {
        return await this._userDal.getAllEmailsIDidntGetByType(emailsToExclude, profileType);
    }
}

export default UserBl;