// /lib/routes/flatMatesRoutes.ts
import {Request, Response, Application} from "express";
import {UserBl} from "../BL/UserBl";
import {UserChoicesBL} from "../BL/UserChoicesBL";
import {SearchBL} from "../BL/SearchBL";
import AlgoBl from "../BL/AlgoBl";

export class Routes {

    public routes(app: Application): void {

        app.route('/')
            .get(async (req: Request, res: Response) => {
                return res.json("INBAL!");
            });

        app.route('/signIn')
            .post(async (req: Request, res: Response) => {
                let userBL = new UserBl();
                try {
                    return res.json(await userBL.signIn(req.body.email, req.body.password));
                } catch (error) {
                    return res.status(400).json(error);
                }
            });

        app.route('/signOut')
            .post(async (req: Request, res: Response) => {
                let userBL = new UserBl();
                try {
                    return res.json(await userBL.signOut(req.body.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/switchAccountState')
            .post(async (req: Request, res: Response) => {
                let userBL = new UserBl();
                try {
                    return res.json(await userBL.switchAccountState(req.body.email, req.body.isDeleted));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });


        app.route('/logOut')
            .post(async (req: Request, res: Response) => {
                let userBL = new UserBl();
                try {
                    return res.json(await userBL.logOut(req.body.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/getTopTenLikedAppartments')
            .get(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.getTopTenLikedAppartments());
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/registerUser')
            .post(async (req: Request, res: Response) => {
                let userBL = new UserBl();
                try {
                    return res.json(await userBL.registerUser(req.body.user));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/updateUser')
            .post(async (req: Request, res: Response) => {
                let userBL = new UserBl();
                try {
                    return res.json(await userBL.updateUser(req.body.user));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/saveNewChoice')
            .post(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.saveNewChoice(req.body.choiceToSave));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/deleteUserChoice')
            .post(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.deleteUserChoice(req.body.choiceToDelete));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/deleteAllUserChoices/:email')
            .get(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.deleteAllUserChoices(req.params.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/saveNewChoices')
            .post(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.saveNewChoices(req.body.userChoices));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/deleteAllUserDislikes/:email')
            .get(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.deleteAllUserDislikes(req.params.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

            app.route('/updateUsersPictures/')
            .get(async (req: Request, res: Response) => {
                let userBL = new UserBl();
                try {
                    return res.json(await userBL.updateUsersPictures());
                } catch (error) {
                    return res.status(500).send(error);
                }
            });            

        app.route('/getLikingUsersByLikedUserEmail/:email')
            .get(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.getLikingUsersByLikedUserEmail(req.params.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/getLikedUsersByLikingUserEmail/:email')
            .get(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.getLikedUsersByLikingUserEmail(req.params.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/checkIfMatch')
            .post(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.checkIfMatch(req.body.firstUserEmail, req.body.secondUserEmail));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/userMatches/:email')
            .get(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.getUserMatches(req.params.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/getLikedApartmentProfilesByLikingUserEmail/:email')
            .get(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.getLikedApartmentProfilesByLikingUserEmail(req.params.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/getLikedRoomateProfilesByLikingUserEmail/:email')
            .get(async (req: Request, res: Response) => {
                let userChoicesBL = new UserChoicesBL();
                try {
                    return res.json(await userChoicesBL.getLikedRoomateProfilesByLikingUserEmail(req.params.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/saveUserPreferences')
            .post(async (req: Request, res: Response) => {
                let userBL = new UserBl();
                try {
                    return res.json(await userBL.saveUserPreferences(req.body.userEmail, req.body.userPreferences));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/changeExistingUserProfileType')
            .post(async (req: Request, res: Response) => {
                let userBL = new UserBl();
                try {
                    return res.send(await userBL.changeExistingUserProfileType(req.body.userEmail, req.body.userNewType, req.body.newProfile, req.body.userPreferences));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/getSearchResult/:email/:maxResults')
            .get(async (req: Request, res: Response) => {
                let searchBL = new SearchBL();
                try {
                    return res.json(await searchBL.getUserSuitableSearchResults(req.params.email, parseInt(req.params.maxResults)));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/getUserByEmail/:email')
            .get(async (req: Request, res: Response) => {
                let userBl = new UserBl();
                try {
                    return res.json(await userBl.getUserByUserEmail(req.params.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });


        app.route('/getApartmentCardPreviewDetailsByUserEmail/:email')
            .get(async (req: Request, res: Response) => {
                let userBl = new UserBl();
                try {
                    return res.json(await userBl.getApartmentCardPreviewDetailsByUserEmail(req.params.email));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route('/getUserMatchesByLastPollDate/:email/:timeDelta')
            .get(async (req: Request, res: Response) => {
                let userChoiceBl = new UserChoicesBL();

                try {
                    return res.json(await userChoiceBl.getUserMatchesByLastPollDate(req.params.email, new Date(Date.now() - 1000 * req.params.timeDelta)));
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route("/calcRecommendationsForRoommatesProfile")
            .get(async (req: Request, res: Response) => {
                let algoBl = new AlgoBl();

                try {
                    return res.json(await algoBl.calcRecommendationsForRoommatesProfile());
                } catch (error) {
                    return res.status(500).send(error);
                }
            });

        app.route("/calcRecommendationsForApartmentsProfile")
            .get(async (req: Request, res: Response) => {
                let algoBl = new AlgoBl();

                try {
                    return res.json(await algoBl.calcRecommendationsForApartmentsProfile());
                } catch (error) {
                    return res.status(500).send(error);
                }
            });
    }
}