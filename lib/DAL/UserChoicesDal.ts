import DbClient from "./DbClient";
import UserChoice from "../shared/Models/UserChoiceFromDb";
import {ChoiceTypeEnum} from "../shared/Enums/ChoiceTypeEnum";
import {DeleteWriteOpResultObject, InsertOneWriteOpResult, InsertWriteOpResult} from "mongodb";
import User from "../shared/Models/UserModel";
import {UserChoiceGroupedByEmail} from "../shared/Models/UserChoiseGroupedByEmail";
import {UserTypeEnum} from "../shared/Enums/UserTypeEnum";
import UserDal from "./UserDal";

export class UserChoicesDal {

    readonly collectionName: string = "UserChoices";
    private userDal: UserDal = new UserDal();

    public async saveNewChoice(choiceToSave: UserChoice) {
        const db = await DbClient.getDbInstance();
        choiceToSave.timestamp = new Date();
        let response: InsertOneWriteOpResult = await db.collection<UserChoice>(this.collectionName).insertOne(choiceToSave);
        return response.insertedCount;
    }

    public async saveNewChoices(userChoices: Array<UserChoice>) {
        const db = await DbClient.getDbInstance();
        let saveDate = new Date();
        userChoices.forEach(choiceToSave => {
            choiceToSave.timestamp = saveDate;
        });

        let response: InsertWriteOpResult = await db.collection<UserChoice>(this.collectionName).insertMany(userChoices);
        return response.insertedCount;
    }

    public async deleteUserChoice(choiceToDelete: UserChoice) {
        const db = await DbClient.getDbInstance();
        let response: DeleteWriteOpResultObject = await db.collection<UserChoice>(this.collectionName).deleteOne(choiceToDelete)
        return response.result.n;
    }

    public async deleteAllUserChoices(userEmail: string) {
        const db = await DbClient.getDbInstance();
        const query = {$or: [{firstUser: userEmail}, {secondUser: userEmail}]};
        let response: DeleteWriteOpResultObject = await db.collection<UserChoice>(this.collectionName).deleteMany(query);
        return response.result.n;
    }

    public async deleteAllUserDislikes(userEmail: string) {
        const db = await DbClient.getDbInstance();
        const query = {
            firstUser: userEmail,
            $or:
                [
                    {choiceType: ChoiceTypeEnum.DISLIKE_APARTMENT},
                    {choiceType: ChoiceTypeEnum.DISLIKE_ROOMMATE}
                ]
        };
        let response: DeleteWriteOpResultObject = await db.collection<UserChoice>(this.collectionName).deleteMany(query);
        return response.result.n;
    }

    public async getLikesByLikingUserEmail(likingUserEmail: string): Promise<UserChoice[]> {
        const db = await DbClient.getDbInstance();
        const query = {
            firstUser: likingUserEmail,
            $or:
                [
                    {choiceType: ChoiceTypeEnum.LIKE_APARTMENT},
                    {choiceType: ChoiceTypeEnum.LIKE_ROOMMATE}
                ]
        };

        return await db.collection<UserChoice>(this.collectionName).find(query).toArray();
    }

    public async getLikedApartmentProfilesByLikingUserEmail(likingUserEmail: string) {
        const db = await DbClient.getDbInstance();
        return await db.collection<UserChoice>(this.collectionName).aggregate([
            {
                $match:
                    {
                        firstUser: likingUserEmail,
                        choiceType: ChoiceTypeEnum.LIKE_APARTMENT
                    }
            },
            {
                $lookup:
                    {
                        from: 'Users',
                        localField: 'secondUser',
                        foreignField: 'email',
                        as: 'likedApartment'
                    }
            },
            {
                $project:
                    {
                        "likedApartment.apartmentProfile.address": 1,
                        "likedApartment.apartmentProfile.size": 1,
                        "likedApartment.apartmentProfile.floor": 1,
                        "likedApartment.apartmentProfile.monthlyRent": 1,
                        "likedApartment.apartmentProfile.roomAmount": 1,
                        "likedApartment.apartmentProfile.hasElevator": 1,
                        "likedApartment.apartmentProfile.hasAirConditioning": 1,
                        "likedApartment.apartmentProfile.hasBalcony": 1,
                        "likedApartment.apartmentProfile.hasGarden": 1,
                        "likedApartment.apartmentProfile.hasParking": 1,
                        "likedApartment.apartmentProfile.hasPets": 1,
                        "likedApartment.apartmentProfile.roommatesAmount": 1

                    }
            }
        ]).toArray();
    }

    public async getLikedRoomateProfilesByLikingUserEmail(likingUserEmail: string) {
        const db = await DbClient.getDbInstance();
        return await db.collection<UserChoice>(this.collectionName).aggregate([
            {
                $match:
                    {
                        firstUser: likingUserEmail,
                        choiceType: ChoiceTypeEnum.LIKE_ROOMMATE
                    }
            },
            {
                $lookup:
                    {
                        from: 'Users',
                        localField: 'secondUser',
                        foreignField: 'email',
                        as: 'likedRoommate'
                    }
            },
            {
                $project:
                    {
                        "likedRoommate.roommateProfile.age": 1,
                        "likedRoommate.roommateProfile.sex": 1,
                        "likedRoommate.roommateProfile.hasPets": 1,
                        "likedRoommate.roommateProfile.isSmoke": 1,
                        "likedRoommate.roommateProfile.isSmokeWeed": 1,
                        "likedRoommate.roommateProfile.religionHabits": 1,
                        "likedRoommate.roommateProfile.eatingHabits": 1
                    }
            }
        ]).toArray();
    }

    public async getLikedUsersByLikingUserEmail(likingUserEmail: string): Promise<UserChoice[]> {
        const db = await DbClient.getDbInstance();
        let result: any = await db.collection<UserChoice>(this.collectionName).aggregate([
            {
                $match:
                    {
                        firstUser: likingUserEmail,
                        $or:
                            [
                                {choiceType: ChoiceTypeEnum.LIKE_APARTMENT},
                                {choiceType: ChoiceTypeEnum.LIKE_ROOMMATE}
                            ]
                    }
            },
            {
                $lookup:
                {
                    from: 'Users',
                    localField: 'secondUser',
                    foreignField: 'email',
                    as: 'likedUser'
                }
             }
            ]).toArray();
            let picturesPromise: Array<Promise<any>> = [];

            for(let res of result) {
                picturesPromise.push(this.userDal.getUserWithPics(res.likedUser[0]));
            }

            let pictures = await Promise.all(picturesPromise);
  
            for (let index = 0; index < result.length; index++) {
                result[index].likedUser[0] = pictures[index];            
            } 

            return result.filter(x => !x.likedUser[0].isDeleted && x.likedUser[0].email!= likingUserEmail);
             
        
    }

    public async getLikingUsersByLikedUserEmail(likedUserEmail: string): Promise<any> {
        const db = await DbClient.getDbInstance();

        let result: any = await db.collection<UserChoice>(this.collectionName).aggregate([
            {
                $match:
                    {
                        secondUser: likedUserEmail
                    }
            },
            {
                $lookup:
                    {
                        from: 'Users',
                        localField: 'firstUser',
                        foreignField: 'email',
                        as: 'likingUser'
                    }
            }
        ]).toArray();

            let picturesPromise: Array<Promise<any>> = [];

            for(let res of result) {
                picturesPromise.push(this.userDal.getUserWithPics(res.likingUser[0]));
            }

            let pictures = await Promise.all(picturesPromise);

            for (let index = 0; index < result.length; index++) {
                result[index].likingUser[0] = pictures[index];            
            } 

            return result.filter(x => !x.likingUser[0].isDeleted);
    }

    public async getLikeByUserEmails(likingUserEmail: string, likedUserEmail: string) {
        const db = await DbClient.getDbInstance();
        const query = {firstUser: likingUserEmail, secondUser: likedUserEmail};
        return await db.collection<UserChoice>(this.collectionName).findOne(query);
    }

    public async getChoicesByUserEmail(userEmail: string): Promise<Array<UserChoice>> {
        const db = await DbClient.getDbInstance();
        const query = {firstUser: userEmail};
        return await db.collection<UserChoice>(this.collectionName).find(query).toArray();
    }

    public async getUsersLikedOrDiskliedByEmail(userEmail: string): Promise<Array<Partial<UserChoice>>> {
        const db = await DbClient.getDbInstance();
        const query = {firstUser: userEmail};

        return await db.collection<UserChoice>(this.collectionName).find(query, {
            fields: {
                "secondUser": 1
            }
        }).toArray();
    }

    public async checkIfMatch(firstEmail: string, secondEmail: string): Promise<boolean> {
        const db = await DbClient.getDbInstance();
        const firstLikeQuery = {
            firstUser: firstEmail,
            secondUser: secondEmail,
            $or:
                [
                    {choiceType: ChoiceTypeEnum.LIKE_APARTMENT},
                    {choiceType: ChoiceTypeEnum.LIKE_ROOMMATE}
                ]
        };
        var firstLike = await db.collection<UserChoice>(this.collectionName).findOne(firstLikeQuery);

        const secondLikeQuery = {
            firstUser: secondEmail,
            secondUser: firstEmail,
            $or:
                [
                    {choiceType: ChoiceTypeEnum.LIKE_APARTMENT},
                    {choiceType: ChoiceTypeEnum.LIKE_ROOMMATE}
                ]
        };
        var secondLike = await db.collection<UserChoice>(this.collectionName).findOne(secondLikeQuery);

        return (firstLike != null && secondLike != null && firstLike.choiceType != secondLike.choiceType);
    }

    public async getAllEmailsThatLikedMeFromList(emails: Array<string>, myEmail: string): Promise<Array<string>> {
        const db = await DbClient.getDbInstance();

        let firstUsersQuery = emails.map((currEmail) => {
            return {
                firstUser: currEmail
            };
        });

        const secondLikeQuery = {
            secondUser: myEmail,
            $and: [
                {
                    $or: [
                        {choiceType: ChoiceTypeEnum.LIKE_APARTMENT},
                        {choiceType: ChoiceTypeEnum.LIKE_ROOMMATE}
                    ]
                },
                {
                    $or: firstUsersQuery
                }

            ]
        };

        let result = await db.collection<UserChoice>(this.collectionName).find(secondLikeQuery).toArray();

        return result.map((currUserChoice) => {
            return currUserChoice.firstUser;
        });
    }

    public async getTopTenLikedAppartments() {
        const db = await DbClient.getDbInstance();
        let result: any = await db.collection<UserChoice>(this.collectionName).aggregate(
            [
                {
                    $match:
                        {
                            choiceType: ChoiceTypeEnum.LIKE_APARTMENT
                        }
                },
                {
                    $group: {
                        _id: "$secondUser",
                        count: {$sum: 1}
                    }
                },
                {
                    $sort: {count: -1}
                },
                {
                    $limit: 10
                },
                {
                    $lookup:
                        {
                            from: 'Users',
                            localField: '_id',
                            foreignField: 'email',
                            as: 'userProfile'
                        }
                }
            ]).toArray();

        let picturesPromise: Array<Promise<any>> = [];

        for(let res of result) {
            picturesPromise.push(this.userDal.getUserWithPics(res.userProfile[0]));
        }

        let pictures = await Promise.all(picturesPromise);

        for (let index = 0; index < result.length; index++) {
            result[index].userProfile[0] = pictures[index];            
        } 

        return result.filter(x => !x.userProfile[0].isDeleted);
    }            

    public async getAllRoommatesUserChoicesGroupedByEmails(): Promise<Array<UserChoiceGroupedByEmail>> {
        const db = await DbClient.getDbInstance();
        return await db.collection<UserChoiceGroupedByEmail>(this.collectionName).aggregate(
            [
                {
                    "$match":
                        {
                            "$or": [
                                {"choiceType": ChoiceTypeEnum.LIKE_APARTMENT},
                                {"choiceType": ChoiceTypeEnum.DISLIKE_APARTMENT}
                            ]
                        }
                },
                {
                    "$group":
                        {
                            "_id": "$firstUser",
                            "profilesIChose":
                                {
                                    "$push":
                                        {
                                            "profile": "$secondUser",
                                            "choiceType": "$choiceType"
                                        }
                                }
                        }
                }
            ], {allowDiskUse: true}).toArray();
    }

    public async getAllApartmentsUserChoicesGroupedByEmails(): Promise<Array<UserChoiceGroupedByEmail>> {
        const db = await DbClient.getDbInstance();
        return await db.collection<UserChoiceGroupedByEmail>(this.collectionName).aggregate(
            [
                {
                    "$match":
                        {
                            "$or": [
                                {"choiceType": ChoiceTypeEnum.LIKE_ROOMMATE},
                                {"choiceType": ChoiceTypeEnum.DISLIKE_ROOMMATE}
                            ]
                        }
                },
                {
                    "$group":
                        {
                            "_id": "$firstUser",
                            "profilesIChose":
                                {
                                    "$push":
                                        {
                                            "profile": "$secondUser",
                                            "choiceType": "$choiceType"
                                        }
                                }
                        }
                }
            ], {allowDiskUse: true}).toArray();
    }
}

export default UserChoicesDal;