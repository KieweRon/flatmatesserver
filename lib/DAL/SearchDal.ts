import {ApartmentPreferencesEnum} from "../shared/Enums/ApartmentPreferencesEnum";
import DbClient from "./DbClient";
import User from "../shared/Models/UserModel";
import {RoommatePreferencesEnum} from "../shared/Enums/RoommatePreferencesEnum";
import UserDal from "./UserDal";
import UserChoicesDal from "./UserChoicesDal";
import UserChoice from "../shared/Models/UserChoiceFromDb";
import {UserTypeEnum} from "../shared/Enums/UserTypeEnum";

export class SearchDal {

    readonly usersCollectionName: string = "Users";
    userDal: UserDal;
    userChoicesDal: UserChoicesDal;

    constructor() {
        this.userDal = new UserDal();
        this.userChoicesDal = new UserChoicesDal();
    }

    public async getSearchResultsByUserPreferences(userEmail: string) {
        let query = {};

        let emailsILikedOrDisliked: Array<Partial<UserChoice>> =
            await this.userChoicesDal.getUsersLikedOrDiskliedByEmail(userEmail);

        if (emailsILikedOrDisliked.length != 0) {
            query = {
                email: {
                    $nin: emailsILikedOrDisliked.map((currChoise) => {
                        return currChoise.secondUser;
                    })
                }
            };
        }

        query["isDeleted"] = false;

        let currUser: Partial<User> = await this.userDal.getUserTypeAndPreferencesByEmail(userEmail);

        if (currUser) {
            if (currUser.type === UserTypeEnum.APARTMENT_SEARCH) {
                query["roommateProfile"] = null;

                if (currUser.userPreferences) {
                    currUser.userPreferences.forEach(filter => {
                        query = this.addApartmentFilterToQuery(filter.preferenceId, filter.preferenceValue, query);
                    });
                }
            } else {
                query["apartmentProfile"] = null;

                if (currUser.userPreferences) {
                    currUser.userPreferences.forEach(filter => {
                        query = this.addRoommateFilterToQuery(filter.preferenceId, filter.preferenceValue, query);
                    });

                    //add age filters
                    let minDate = new Date();
                    let maxDate = new Date();
                    minDate.setFullYear(minDate.getFullYear() - currUser.userPreferences.find(x=> x.preferenceId == RoommatePreferencesEnum.MinAge).preferenceValue);
                    maxDate.setFullYear(maxDate.getFullYear() - currUser.userPreferences.find(x=> x.preferenceId == RoommatePreferencesEnum.MaxAge).preferenceValue);
                    
                    query["roommateProfile.date"] = {"$lt":minDate.getTime(),"$gte":maxDate.getTime() };
                }
            }
        }

        let usersAfterFilters = await this.executeQuery(query);

        let emailsAfterFilters = usersAfterFilters.map((currUser) => {
            return currUser.email;
        });
        return {
            emailsAfterFilters: emailsAfterFilters,
            userType: currUser.type
        };
    }

    private async executeQuery(query: any): Promise<Array<Partial<User>>> {
        const db = await DbClient.getDbInstance();

        let result = await db.collection<User>(this.usersCollectionName).find(query, {
            fields: {
                "email": 1
            }
        }).toArray();
        
        if (result && result.length > 0)
        {
            result = await this.userDal.getUsersWithPics(result);
        }
        return result;
    }

    private addApartmentFilterToQuery(preferenceId: number, preferenceValue: any, baseQuery: any) {
        switch (preferenceId) {
            case ApartmentPreferencesEnum.AirConditionOnly: {
                baseQuery["apartmentProfile.hasAirConditioning"] = preferenceValue;
                break;
           }
           case ApartmentPreferencesEnum.Cities:
           {
                   baseQuery["apartmentProfile.city"] = { "$in": preferenceValue }  ;
                break;
            }
            case ApartmentPreferencesEnum.ElevatorOnly: {
                baseQuery["apartmentProfile.hasElevator"] = preferenceValue;
                break;
            }
            case ApartmentPreferencesEnum.MaxSize: {
                baseQuery["apartmentProfile.size"] = {"$lte": preferenceValue};
                break;
            }
            case ApartmentPreferencesEnum.MaxFloor: {
                baseQuery["apartmentProfile.floor"] = {"$lte": preferenceValue};
                break;
            }
            case ApartmentPreferencesEnum.MaxMonthlyRent: {
                baseQuery["apartmentProfile.monthlyRent"] = {"$lte": preferenceValue};
                break;
            }
            case ApartmentPreferencesEnum.MaxRoommatesNumber: {
                baseQuery["apartmentProfile.roommates"] = {$size: {$lte: preferenceValue}};
                break;
            }
            case ApartmentPreferencesEnum.MinRoommatesNumber: {
                baseQuery["apartmentProfile.roommates"] = {$size: {$gte: preferenceValue}};
                break;
            }
            case ApartmentPreferencesEnum.MinSize: {
                baseQuery["apartmentProfile.size"] = {"$gte": preferenceValue};
                break;
            }
            case ApartmentPreferencesEnum.WithParking: {
                baseQuery["apartmentProfile.hasParking"] = preferenceValue;
                break;
            }
            case ApartmentPreferencesEnum.RoomsNumber: {
                baseQuery["apartmentProfile.roomAmount"] = {"$eq": preferenceValue};
                break;
            }
            case ApartmentPreferencesEnum.WithBalcony: {
                baseQuery["apartmentProfile.hasBalcony"] = preferenceValue;
                break;
            }
        }

        return baseQuery;
    }

    private addRoommateFilterToQuery(preferenceId: number, preferenceValue: any, baseQuery: any) {
        switch (preferenceId) {
            case RoommatePreferencesEnum.EatKosher: {
                baseQuery["roommateProfile.eatingHabits"] = {"$in": ["KOSHER", "KOSHER_LE_MEHADRIN"]};
                break;
            }
            case RoommatePreferencesEnum.Sex: {
                baseQuery["roommateProfile.sex"] = preferenceValue;
                break;
            }
            case RoommatePreferencesEnum.HasPets:
            {
                baseQuery["roommateProfile.hasPets"] = preferenceValue;
                break;
            }
            case RoommatePreferencesEnum.IsSmoking: {
                baseQuery["roommateProfile.isSmoke"] = preferenceValue;
                break;
            }
            case RoommatePreferencesEnum.IsWeedFriendly: {
                baseQuery["roommateProfile.isSmokeWeed"] = preferenceValue;
                break;
            }
        }


        return baseQuery ;
    }
}