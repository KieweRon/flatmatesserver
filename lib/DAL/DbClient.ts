import {MongoClient, Db} from "mongodb";
import config from '../../config/config';

class DbClient {

    public db: Db;

    private async connect() {
        const conf = config;
        let mongoClient = await MongoClient.connect(conf.dbConfig.uri);
        this.db = mongoClient.db(conf.dbConfig.dbName);
        console.log("Connected to db");
        return this.db;
    }

    public async getDbInstance(): Promise<Db> {
        if (this.db) {
            return new Promise<Db>((res, rej) => {
                return res(this.db);
            })
        } else {
            console.log(this)
            await this.connect();
            return this.db;
        }
    }
}

const DbClientInstance = new DbClient();
export default DbClientInstance;