import DbClient from "./DbClient";
import {InsertWriteOpResult} from "mongodb";
import AlgoRecommendationResults from "../shared/Models/AlgoRecommendationResults";
import AlgoLabelingResults from "../shared/Models/AlgoLabelingResults";
import {EmailWithCounter} from "../shared/Models/EmailWithCounter";

export class AlgoDal {
    readonly roommatesRecommendationsCollections: string = "RoommatesRecommendations";
    readonly apartmentsRecommendationsCollections: string = "ApartmentsRecommendations";
    readonly roommatesLabelsCollections: string = "RoommatesLabels";
    readonly apartmentsLabelsCollections: string = "ApartmentsLabels";

    public async saveRecommendationsForRoommatesProfilesInDB(algoResults: Array<AlgoRecommendationResults>): Promise<number> {
        if (algoResults.length > 0) {

            const db = await DbClient.getDbInstance();

            await db.collection(this.roommatesRecommendationsCollections).deleteMany({});

            let insertResponse: InsertWriteOpResult =
                await db.collection(this.roommatesRecommendationsCollections).insertMany(algoResults);

            return insertResponse.result.n;
        }

        return 0;
    }

    public async saveRoommatesLabelsInDB(algoResults: Array<AlgoLabelingResults>): Promise<number> {
        if (algoResults.length > 0) {
            const db = await DbClient.getDbInstance();

            await db.collection(this.roommatesLabelsCollections).deleteMany({});

            let insertResponse: InsertWriteOpResult =
                await db.collection(this.roommatesLabelsCollections).insertMany(algoResults);
            return insertResponse.result.n;
        }

        return 0;
    }

    public async getRoommateLabelByEmail(email: string): Promise<number> {
        const db = await DbClient.getDbInstance();

        let result: AlgoLabelingResults =
            await db.collection(this.roommatesLabelsCollections).findOne({email: email});

        return result.label;
    }

    public async getRoommateZeroLabel(): Promise<number> {
        const db = await DbClient.getDbInstance();

        let result: AlgoLabelingResults =
            await db.collection(this.roommatesRecommendationsCollections).findOne({isCenter: true}, {
                fields: {
                    label: true
                }
            });

        return result.label;
    }

    public async saveRecommendationsForApartmentsProfilesInDB(algoResults: Array<AlgoRecommendationResults>):
        Promise<number> {
        const db = await DbClient.getDbInstance();

        await db.collection(this.apartmentsRecommendationsCollections).deleteMany({});

        let insertResponse: InsertWriteOpResult =
            await db.collection(this.apartmentsRecommendationsCollections).insertMany(algoResults);

        return insertResponse.result.n;
    }

    public async saveApartmentsLabelsInDB(algoResults: Array<AlgoLabelingResults>): Promise<number> {
        const db = await DbClient.getDbInstance();

        await db.collection(this.apartmentsLabelsCollections).deleteMany({});

        let insertResponse: InsertWriteOpResult =
            await db.collection(this.apartmentsLabelsCollections).insertMany(algoResults);

        return insertResponse.result.n;
    }

    public async getApartmentLabelByEmail(email: string): Promise<number> {
        const db = await DbClient.getDbInstance();

        let result: AlgoLabelingResults =
            await db.collection(this.apartmentsLabelsCollections).findOne({email: email});

        return result.label;
    }

    public async getApartmentZeroLabel(): Promise<number> {
        const db = await DbClient.getDbInstance();

        let result: AlgoLabelingResults =
            await db.collection(this.apartmentsRecommendationsCollections).findOne({isCenter: true}, {
                fields: {
                    label: true
                }
            });

        return result.label;
    }

    public async getRoommateRecommendationsByLabel(label: number,
                                                   emailsAfterFilters: Array<string>):
        Promise<Array<EmailWithCounter>> {
        const db = await DbClient.getDbInstance();

        let profilesFieldName = "profile";

        let query = [
            {
                $match: {
                    label: label
                }
            }
        ];

        let orQuery = emailsAfterFilters.map((currEmail) => {
            return {
                $eq: [
                    `$$${profilesFieldName}.email`, currEmail
                ]
            }
        });

        query.push({
            $project: {
                profiles: {
                    $filter: {
                        input: "$profiles",
                        as: profilesFieldName,
                        cond: {
                            $or: orQuery
                        }
                    }
                }
            }
        } as any);

        let results = await db.collection<AlgoRecommendationResults>(this.roommatesRecommendationsCollections)
            .aggregate(query).toArray();

        return results[0].profiles;
    }

    public async getApartmentRecommendationsByLabel(label: number,
                                                    emailsAfterFilters: Array<string>):
        Promise<Array<EmailWithCounter>> {
        const db = await DbClient.getDbInstance();

        let profilesFieldName = "profile";

        let query = [
            {
                $match: {
                    label: label
                }
            }
        ];

        let orQuery = emailsAfterFilters.map((currEmail) => {
            return {
                $eq: [
                    `$$${profilesFieldName}.email`, currEmail
                ]
            }
        });

        query.push({
            $project: {
                profiles: {
                    $filter: {
                        input: "$profiles",
                        as: profilesFieldName,
                        cond: {
                            $or: orQuery
                        }
                    }
                }
            }
        } as any);

        let results = await db.collection<AlgoRecommendationResults>(this.apartmentsRecommendationsCollections)
            .aggregate(query).toArray();

        return results[0].profiles;
    }

    public async addNewRoommatesToLabels(email: string): Promise<number> {
        const db = await DbClient.getDbInstance();
        let zeroLabel = await this.getRoommateZeroLabel();

        let result = await db.collection(this.roommatesLabelsCollections).insertOne({
                email: email,
                label: zeroLabel
            }
        );

        return result.result.n;
    }

    public async addNewApartmentToLabels(email: string): Promise<number> {
        const db = await DbClient.getDbInstance();
        let zeroLabel = await this.getRoommateZeroLabel();

        let result = await db.collection(this.apartmentsLabelsCollections).insertOne({
                email: email,
                label: zeroLabel
            }
        );

        return result.result.n;
    }
}

export default AlgoDal;