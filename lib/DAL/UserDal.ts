import DbClient from "./DbClient";
import UserModel from "../shared/Models/UserModel";
import User from "../shared/Models/UserModel";
import {UserPreferences} from "../shared/types/UserPreferencesType";
import {FilterQuery, FindAndModifyWriteOpResultObject, InsertOneWriteOpResult, WriteOpResult} from "mongodb";
import {UserTypeEnum} from "../shared/Enums/UserTypeEnum";
import * as fs from 'fs';
import * as mkdirp from 'mkdirp';

export class UserDal {

    private readonly base_path: string = "/home/project63/flatPics/";
    readonly collectionName: string = "Users";
    private lastFemaleRoommatePicNumber = 1;
    private lastMaleRoommatePicNumber = 1;
    private lastApartmentPicNumber = 1;

    public async getUserByEmail(userEmail: string): Promise<UserModel> {
        const db = await DbClient.getDbInstance();
        const query = {email: userEmail};
        let user: User = await db.collection<UserModel>(this.collectionName).findOne(query);

        return await this.getUserWithPics(user);
    }

    public async getUsersByEmails(userEmails: Array<string>): Promise<Array<UserModel>> {
        const db = await DbClient.getDbInstance();
        const query = {email: {$in: userEmails}};
        let users: Array<User> = await db.collection<UserModel>(this.collectionName).find(query).toArray();

        return await this.getUsersWithPics(users);
    }

    public async getUserWithPics(user: User) {
        if (user != null) {
            if (user.apartmentProfile != null) {
                user.apartmentProfile.picture = await this.getPictureBase64ByUrl(user.apartmentProfile.picture);
                let picturesPromise: Array<Promise<any>> = [];

                for (let roommate of user.apartmentProfile.roommates) {
                    picturesPromise.push(this.getPictureBase64ByUrl(roommate.picture));
                }

                let pictures = await Promise.all(picturesPromise);

                for (let index = 0; index < user.apartmentProfile.roommates.length; index++) {
                    user.apartmentProfile.roommates[index].picture = pictures[index];
                }
            } else if (user.roommateProfile != null) {
                user.roommateProfile.picture = await this.getPictureBase64ByUrl(user.roommateProfile.picture);
            }
        }
        return user;
    }

    public async getUsersWithPics(users: User[]) {
        let usersPromise: Array<Promise<User>> = [];

        for (let userIndex = 0; userIndex < users.length; userIndex++) {
            usersPromise.push(this.getUserWithPics(users[userIndex]));
        }

        return await Promise.all(usersPromise);
    }

    public async getPictureBase64ByUrl(pictureUrl: string): Promise<string> {
        let fileType: string = pictureUrl.split('.').pop();
        let imageData: string = "data:image/" + fileType + ";base64,";

        return new Promise((resolve, reject) => {            
            fs.readFile(this.base_path + pictureUrl, function (err, data) {
                if (err) {
                    resolve("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU0AAAFNCAYAAACE8D3EAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAABI/SURBVHhe7d2NbxTHGQfgMTYQoBiMcYkDJSEN+VASJa2qSv3/pVSJilS1SUpDgoFCgGDiYoMJYKAep1Ypwb59fbt7O7vPSagfeW9n5pmXX/bu9vam1u6vP08eBAgQIFBJYF+lKkUECBAgsCUgNDUCAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJCM4CllAABAkJTDxAgQCAgIDQDWEoJECAgNPUAAQIEAgJTa/fXnwfqlTYgsLr2ID1Y/yk9ebKx9cejHwL7pvel1w4eSEcOv7b551Ca3vzfHuULCM0J7eHTp8/SD8sr6eatu+mnR48nNAvDtiWwf2YmnTx5LJ1+/WQ6cGB/W8MapwEBodkA6qhD5jPLby/fEJajoHr4z6f2TaV3zp1OC/PHe7i6YSxJaLa8z3fu/jtd+u56y6MarmsCi6/Pp3NnF7s2LfOpIOBNlgpIdZUIzLokyz9Oflvm2vXb5S9kgCsQmi1t+uPHT9K3SzdaGs0wJQhc//5OWrm3VsJUzfEFAaHZUjtcvnozPX/mQoWWuIsZZunKzWLmaqI/CwjNFjph/eGj9OPKagsjGaI0gXzlRH7bxqMcAaHZwl4t+0vRgnK5Q9y+s1Lu5Ac4c5+et7DpX15cSqurD0aOlC+EPjZ7ZGSdgjIEHm6eRVbZ93wZ0p/+8GEZizLLJDRbaII//+Wrke9nnpibTe+fP9vCbAzRpkD+hDx/4DPq8enH59PhQwdHlfnnHRDw8ryFTajyAdAbm9ftefRP4PTiQqVFbWz4+mwlqA4UCc2GNyF/XbLKY2bza3Ye/RPwffP+7anQ7N+eWhEBAg0KCM0GcR2aAIH+CQjN/u2pFREg0KCA0GwQ16EJEOifgNDs355aEQECDQoIzQZxJ3no/Kl9vm9nviFE/pP/e75piAcBAuMJuLh9PL+Rz87h9fmFr0fW1XFxc/6Oe/7K5vLdezve4Dh/62hu7mg6tXDCxdQjd6Wegs+++HLkgT764FyaPerbYCOhOlDgTLMDmzDuFPIZ5MVL19Jf/35p69snu/18Rv5n+V6OuTY/x9nnuPqePzQBoVn4juc75Fz42zd7uotSvvNSfq677BTeBKbfqoDQbJW73sFu3r679dMZVb6mudPI+bn5GO4iXu/eOFp/BYRmoXubA3Np88bGdT3yy/p8TA8CBHYXEJoFdkj+JPzKv27VPvMcwvnYHgQI7CwgNAvsjvzzv+O8JN9tyfnYVW8yUiCdKRMYW0Bojk3Y7gHyhza7fTo+7mzysX9YdifxcR09v78CQrOwvW3jpxHyJUkeBAi8WkBoFtQZ+eL1Kj+fMO6S8tlmHsuDAIFfCgjNgrri3ur91mbb5litLcpABGoQEJo1ILZ1iCbfy3x5DevrP7W1rLHHyZ/4u850bEYHqCggNCtCdaHs0aP2brjxZONpF5Y8cg45ML/655Wtr48uXavvutWRAysYrIDQHOzW777wjafdD83twNy+/Cp/gCU4NXTTAkKzaeFCjz8zPd3pmb8cmNuTFZyd3rZeTE5oFrSNBw/ub222bY4VXdROgSk4o5Lq9yIgNPeiNqHn/OrIodZGzvfd7OIj31A5v4c56htRzji7uHv9mJPQLGgfj7V4k9r5udnOyeTAzPcAHRWYzjg7t3W9mpDQLGg7DxzYn2Znm7+7dx4jj9WlRzQwBWeXdq9fcxGahe3n6cWTjc/41MJc42NEBthrYArOiLLaqgJCs6pUR+rmjh1t9Gwzn2UuzB/vyGrT1o/CRV6S7zRx73F2ZkuLn4jQLHAL337zjTS1b6r2medj5mN35VFXYDrj7MqO9mMeQrPAfTx86GB6//zZ2mf+zrnTnfmFyroDU3DW3i6DPaDQLHTr88v08789U9sZZz5WV16WNxWYgrPQZu/YtIVmxzYkMp0cch++91Ya55rK/Nz8m9tDCUzBGekwta8SEJqF98Xs5rWbn3z0TjrzxkLorDO/f5mfk5+bj9GFR9NnmC+v0YdDXdj18uYgNMvbs1/MeHp6Xzp75lT64+8+2HrJfmLzwvT9MzO/qMv/X/5n595c3KrNz8nP7cKj7cB0xtmFXS9zDlNr99eflzn1Mmadf6Ts8wtfj5zspx+fr/1DmDz20827FW1s/ufMZjh27YL1bZRJBeaLm7L4+nw6d3Zx5D7tpeCzL74c+bT8FklXzvhHTnbgBd04zRj4JjS1/HwWmYMyf9ouMHdX9lK9qS7s33GFZv/2tJgVdeEM80UswVlM60x0okJzovzDHbxrgek9zuH2YnTlQjMqpn5sga4GpuAce2sHcQChOYht7s4iux6YgrM7vdLVmQjNru5MD+dVSmAKzh42X41LEpo1YjrUzgKlBabg1M07CQhNvdG4QKmBKTgbb40iBxCaRW5bvZNef/go3bx9t96D/vdopQem4GykLYo+qNAsevvGn3wOzK/+sZSWrt6sPTj7EpiCc/w+69MRhGafdjO4lu3AfLKxsfXMOoOzb4EpOIPN1eNyodnjzd1taS8H5nZtHcHZ18AUnAP9y/LSsoXmAPtgp8CsIzj7HpiCc4B/YYTmsDd9VGCOE5xDCcxto+Xle8NupoGu3pnmgDa+amDuJTiHFpgDahtLdaY5zB6IBmYkOAXmMHtqqKt2pjmAnd9rYFYJToE5gAayxP8TEJo9b4hxA3O34BSYPW8ey3ulgNDscWPUFZivCk6B2ePGsbRdBYRmTxuk7sB8MTi/W7qRLl66lp4/8/NSPW0fy9pFQGj2sD2aCsxtqtt3VgRmD/vGkqoJCM1qTsVUNR2YxUCYKIGGBIRmQ7CTOKzAnIS6MYcmIDR7suMCsycbaRmdFxCand+i0RMUmKONVBCoS0Bo1iU5oeMIzAnBG3awAkKz4K0XmAVvnqkXKyA0C906gVnoxpl28QJCs8AtFJgFbpop90ZAaBa2lQKzsA0z3d4JCM2CtlRgFrRZptpbAaFZ0NZevvp92v4RtIKmbaoEeiUgNHu1nRZDgEDTAkKzaWHHJ0CgVwJCs1fbaTEECDQtIDSbFnZ8AgR6JSA0e7WdFkOAQNMCQrNpYccnQKBXAkKzV9tpMQQINC0gNJsWdnwCBHolIDR7tZ0WQ4BA0wJCs2lhxydAoFcCQrNX22kxBAg0LSA0mxZ2fAIEeiUwtXZ//XmvVtSxxTx9+ix9fuHrkbP69OPz6fChg7vWfbd0I91bfTDyWAraE/j9J++OHOyzL74cWfPRB+fS7NEjI+sUTF5AaDa8B3WGZsNTdfiGBIRmQ7ATOqyX5xOCNywBAmUKCM0y982sCRCYkIDQnBC8YQkQKFNAaJa5b2ZNgMCEBITmhOANS4BAmQJCs8x9M2sCBCYkIDQbhp+erkb8YP1hwzNx+EkI5F8QrfKYmZmpUqamAwLV/kZ3YKIlT+G1gwdGTv/K1Vtp5d5aytd1evRDYHXtQbr4zdVKi5mp+C/XSgdT1KiAi9sb5f354BcvXUs/rqy2MJIhShTI/1Kt8s2iEtfWxzk702xhV+dPzLYwiiFKFZibO1rq1Ac5b6HZwrafOD6b9nvPqgXpMoc4tXCizIkPdNZCs4WNzx8GnTm90MJIhihN4NTC3MgbtZS2pr7PV2i2tMOLp+bT7Ky72LTEXcQw+dXHb07/uoi5muT/BIRmi93w7ttnUpVP0luckqEmJDC1byq9d/436cCB/ROagWH3KuDT873K7fF5jx8/Sd9cvp5W3Rdzj4LlPy3/i/P9d9/0srzQrRSaE9q4a9dvpxu3ltPzZ+4BPaEtmMiw+T3Mt84upqpfepjIJA26q4DQnGCD5AvZf1heSXc3r+HcvIO+AJ3gXjQ5dD6zPDl/bPPPcWeXTUK3dGyh2RJ0lWHyS/cN3wiqQlVETf6Wz/T0tLPKInar+iSFZnUrlQQIEEg+PdcEBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECAhNPUCAAIGAgNAMYCklQICA0NQDBAgQCAgIzQCWUgIECPwH3+K0Nko1+5IAAAAASUVORK5CYII=");
                } else {
                    // convert binary data to base64 encoded string
                    resolve(imageData + (new Buffer(data)).toString('base64'));
                }
            });

        });
    }

    public async getAllApartmentSearchUsersForAlgo(): Promise<Array<Partial<UserModel>>> {
        const db = await DbClient.getDbInstance();
        const query = {
            roommateProfile: null
        };

        return await db.collection<UserModel>(this.collectionName).find(query, {
            fields: {
                "email": 1,
                // "apartmentProfile.city": 1,
                "apartmentProfile.size": 1,
                "apartmentProfile.floor": 1,
                "apartmentProfile.monthlyRent": 1,
                "apartmentProfile.roomAmount": 1,
                "apartmentProfile.hasElevator": 1,
                "apartmentProfile.hasAirConditioning": 1,
                "apartmentProfile.hasBalcony": 1,
                "apartmentProfile.hasGarden": 1,
                "apartmentProfile.hasParking": 1,
                "apartmentProfile.hasPets": 1,
                "apartmentProfile.roommatesAmount": 1
            }
        }).toArray();
    }

    public async getAllEmailsIDidntGetByType(emailsToExclude: Array<string>, profileType: UserTypeEnum):
        Promise<Array<Partial<UserModel>>> {

        const db = await DbClient.getDbInstance();

        let query: FilterQuery<User> = {
            type: profileType
        };

        if (emailsToExclude.length != 0) {
            query.email = {
                $nin: emailsToExclude
            };
        }
        return await db.collection<UserModel>(this.collectionName).find(query, {
            fields: {
                "email": 1
            }
        }).toArray();
    }

    public async getAllRoommatesSearchUsersForAlgo(): Promise<Array<Partial<UserModel>>> {
        const db = await DbClient.getDbInstance();
        const query = {
            apartmentProfile: null
        };

        return await db.collection<UserModel>(this.collectionName).find(query, {
            fields: {
                "email": 1,
                "roommateProfile.age": 1,
                "roommateProfile.sex": 1,
                "roommateProfile.hasPets": 1,
                "roommateProfile.isSmoke": 1,
                "roommateProfile.isSmokeWeed": 1,
                "roommateProfile.religionHabits": 1,
                "roommateProfile.eatingHabits": 1
            }
        }).toArray();
    }

    public async getApartmentCardPreviewDetailsByUserEmail(userEmail: string): Promise<UserModel> {
        const db = await DbClient.getDbInstance();
        const query = {email: userEmail};
        const select = {
            email: 1,
            apartmentProfile: 1,
            "apartmentProfile.monthlyRent": 1,
            "apartmentProfile.picture": 1,
            "apartmentProfile.address": 1,
            "apartmentProfile.roomAmount": 1,
            "apartmentProfile.size": 1
        };

        let user: UserModel = await db.collection<UserModel>(this.collectionName).find(query).project(select).limit(1).toArray()[0];
        if (user != null && user.apartmentProfile != null) {
            user.apartmentProfile.picture = await this.getPictureBase64ByUrl(user.apartmentProfile.picture);
        }
        return user;
    }

    public async signIn(userEmail: string, password: string) {
        const db = await DbClient.getDbInstance();
        const query = {email: userEmail, password: password};
        const response: FindAndModifyWriteOpResultObject<UserModel> = await db.collection<UserModel>(this.collectionName).findOneAndUpdate(
            query,
            {$set: {isDeleted: false, isActive: true, lastModified: new Date()}},
            {returnOriginal: false});

        let user: any = response.value;
        if (user != null) {
            user = await this.getUserWithPics(user);
            delete user._id;
        }
        return user;
    }

    public async saveUserPreferences(userEmail: string, userPreferences: UserPreferences) {
        const db = await DbClient.getDbInstance();
        const query = {email: userEmail};

        const response: FindAndModifyWriteOpResultObject<UserModel> = await db.collection<UserModel>(this.collectionName).findOneAndUpdate(
            query,
            {$set: {userPreferences: userPreferences}},
            {returnOriginal: false});
        return response.ok;
    }

    public async saveNewUser(userToSave: User) {
        const db = await DbClient.getDbInstance();
        const userQuery = {email: userToSave.email};
        var currUser = await db.collection<UserModel>(this.collectionName).findOne(userQuery);

        //check if already registered
        if (!currUser) {
            //save the picture to fileSystem and get the file name to save on user in db
            if (userToSave.apartmentProfile != null) {
                userToSave.apartmentProfile.picture = this.saveUserPictureToFileSystem(userToSave.email, userToSave.apartmentProfile.picture);
                for (let index = 0; index < userToSave.apartmentProfile.roommates.length; index++) {
                    userToSave.apartmentProfile.roommates[index].picture = this.saveUserPictureToFileSystem(userToSave.email, userToSave.apartmentProfile.roommates[index].picture, index + 1);
                }
            } else {
                userToSave.roommateProfile.picture = this.saveUserPictureToFileSystem(userToSave.email, userToSave.roommateProfile.picture);
            }

            userToSave.isDeleted = false;
            const insertUserQuery = userToSave;
            let response: InsertOneWriteOpResult = await db.collection<UserModel>(this.collectionName).insertOne(insertUserQuery);

            return response.insertedCount;
        } else {
            throw new Error("Already Exists");
        }
    }

    public saveUserPictureToFileSystem(email: string, base64: string, roommateIndex?: number): string {
        var imageData: any = this.decodeBase64Image(base64);
        var roommatePrefix = roommateIndex ? "roommate_" + roommateIndex + "_" : "";
        var fileName = roommatePrefix + email.split('@')[0] + "." + imageData.type;
        var filePath = this.base_path + fileName;

        mkdirp(this.base_path, function (err) {
            if (err) return (err);

            fs.writeFile(filePath, imageData.data, 'base64', function (err) {
                if (err) {
                    throw err;
                }
            });

        });


        return fileName;
    }

    private decodeBase64Image(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
        var response: any = {};

        if (matches.length !== 3) {
            return new Error('Invalid input string');
        }

        response.type = matches[1].split('/')[1];
        response.data = new Buffer(matches[2], 'base64');

        return response;
    }

    public async updateUser(userToUpdate: User): Promise<number> {
        const db = await DbClient.getDbInstance();
        const userQuery = {email: userToUpdate.email};

        var currUser = await db.collection<UserModel>(this.collectionName).findOne(userQuery);

        if (currUser) {
            //save the picture to fileSystem and get the file name to save on user in db
            if (userToUpdate.apartmentProfile != null) {
                userToUpdate.apartmentProfile.picture = this.saveUserPictureToFileSystem(userToUpdate.email, userToUpdate.apartmentProfile.picture);
                for (let index = 0; index < userToUpdate.apartmentProfile.roommates.length; index++) {
                    userToUpdate.apartmentProfile.roommates[index].picture = this.saveUserPictureToFileSystem(userToUpdate.email, userToUpdate.apartmentProfile.roommates[index].picture, index + 1);
                }
            } else {
                userToUpdate.roommateProfile.picture = this.saveUserPictureToFileSystem(userToUpdate.email, userToUpdate.roommateProfile.picture);
            }
            let response: WriteOpResult = await db.collection<UserModel>(this.collectionName).update(
                {email: userToUpdate.email}, userToUpdate);
            return response.result.nModified;
        } else {
            return 0;
        }
    }
    public async getNextFemaleRoommatePic():Promise<string>
    {
        let picName = "images (" + this.lastFemaleRoommatePicNumber + ").jpg";
        const roommatesFolder = 'd:/testpics/rommates/female/';
        let fullUrl = roommatesFolder + picName;
        this.lastFemaleRoommatePicNumber++;
        if (this.lastFemaleRoommatePicNumber >= 100)
        {
            this.lastFemaleRoommatePicNumber = 1;
        }
        let b =  await this.getPictureBase64ByUrl(fullUrl);   
        return b;              
    }
    public async getNextMaleRoommatePic():Promise<string>
    {
        let picName = "images (" + this.lastMaleRoommatePicNumber + ").jpg";
        const roommatesFolder = 'd:/testpics/rommates/male/';
        let fullUrl = roommatesFolder + picName;
        this.lastMaleRoommatePicNumber++;
        if (this.lastMaleRoommatePicNumber >= 100)
        {
            this.lastMaleRoommatePicNumber = 1;
        }
        let b =  await this.getPictureBase64ByUrl(fullUrl);   
        return b;              
    }

    public async getNextApartmentPic():Promise<string>
    {
        let picName = "images (" + this.lastApartmentPicNumber + ").jpg";
        const aparmentFolder = 'd:/testpics/flats/';
        let fullUrl = aparmentFolder + picName;
        this.lastApartmentPicNumber++;
        if (this.lastApartmentPicNumber >= 73)
        {
            this.lastApartmentPicNumber = 1;
        }
        let b = await this.getPictureBase64ByUrl(fullUrl); 
        return b;                
    }

    public async updateUsersPictures(){
        const db = await DbClient.getDbInstance();
         let users = await db.collection<UserModel>(this.collectionName).find().toArray();
         for (let userToUpdate of users) {                     
            if (userToUpdate.apartmentProfile != null) {
                userToUpdate.apartmentProfile.picture = await this.getNextApartmentPic();
                for (let index = 0; index < userToUpdate.apartmentProfile.roommates.length; index++) {
                    if (userToUpdate.apartmentProfile.roommates[index].sex == "female")
                    {
                        userToUpdate.apartmentProfile.roommates[index].picture = await this.getNextFemaleRoommatePic()
                    }
                    else
                    {
                        userToUpdate.apartmentProfile.roommates[index].picture = await this.getNextMaleRoommatePic()
                    }
                }
            } 
            else
            {
                if (userToUpdate.roommateProfile.sex == "female")
                {
                    userToUpdate.roommateProfile.picture = await this.getNextFemaleRoommatePic();
                }
                else
                {
                    userToUpdate.roommateProfile.picture = await this.getNextMaleRoommatePic();
                }
            }
            await this.updateUser(userToUpdate);
         };

    }

    public async signOut(email: string) {

        const db = await DbClient.getDbInstance();

        const query = {email: email};

        const response: FindAndModifyWriteOpResultObject<UserModel> = await db.collection<UserModel>(this.collectionName).findOneAndUpdate(
            query,
            {$set: {isActive: false, lastModified: new Date()}},
            {returnOriginal: false});

        return response.ok;
    }

    public async switchAccountState(userEmail: string, isDeleted: boolean) {
        return await this.updateUserBySetQueryObj(userEmail, {$set: {isDeleted: isDeleted}});
    }

    public async updateUserBySetQueryObj(userEmail: string, setQueryObj: any) {
        const db = await DbClient.getDbInstance();
        const query = {email: userEmail};
        const response: FindAndModifyWriteOpResultObject<UserModel> = await db.collection<UserModel>(this.collectionName).findOneAndUpdate(
            query,
            setQueryObj,
            {returnOriginal: false});

        return !!response.value;
    }

    public async getUserTypeAndPreferencesByEmail(email: string): Promise<Partial<User>> {
        const db = await DbClient.getDbInstance();

        return await db.collection(this.collectionName).findOne({email: email}, {
            fields: {
                "type": 1,
                "userPreferences": 1
            }
        });
    }

    public async getAllUserTypeAndPreferencesByType(userType: UserTypeEnum): Promise<Array<Partial<User>>> {
        const db = await DbClient.getDbInstance();

        return await db.collection(this.collectionName).find({type: userType}, {
            fields: {
                "type": 1,
                "userPreferences": 1,
                "email": 1
            }
        }).toArray();
    }
}

export default UserDal;