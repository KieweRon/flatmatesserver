import app from "./app";
import AlgoBl from "./BL/AlgoBl";
import * as NodeSchedule from "node-schedule";

const PORT = 3000;

app.listen(PORT, () => {
    console.log('Express server listening on port ' + PORT);
});

const algoBl: AlgoBl = new AlgoBl();

const scheduleJob = NodeSchedule.scheduleJob('00 00 00 * * *', async (date) => {
    console.log('Algo running ' + date);

    try {
        await algoBl.calcRecommendationsForRoommatesProfile();
        await algoBl.calcRecommendationsForApartmentsProfile();
    } catch (e) {
        console.log(e);
    }

    console.log('Algo finish ' + new Date());
});