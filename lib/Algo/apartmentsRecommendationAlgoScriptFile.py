#!/usr/bin/env python
# coding: utf-8
import numpy as np
import pandas as pd
import sys
import pickle
import os
import json

all_profiles_choices_file = sys.argv[1]
all_profiles_file = sys.argv[2]
labels_with_counters_results_file_name = sys.argv[3]
emails_with_labels_results_name = sys.argv[4]

all_profiles_choices = pd.read_json(all_profiles_choices_file)
all_profiles = pd.read_json(all_profiles_file)

all_profiles_dict = all_profiles.set_index('email').T.to_dict('dict')

keys = list(next(iter(all_profiles_dict.values())).keys())
columns = ["_id", "counter"]
columns = columns + keys

all_profiles_i_liked = pd.DataFrame(columns=columns)

for index, curr_email in all_profiles_choices.iterrows():
    curr_row = []

    for key_index, curr_key in enumerate(columns):
        if curr_key == "_id":
            curr_row.append(curr_email._id)
        else:
            curr_row.append(0)

    for inner_index, curr_profile in enumerate(curr_email.profilesIChose):
        if curr_profile.get("choiceType") == 2 or curr_profile.get("choiceType") == 1:
            user_i_likend = curr_profile.get("profile")
            for key_index, curr_key in enumerate(columns):
                if curr_key == "counter":
                    curr_row[key_index] = curr_row[key_index] + 1
                elif curr_key != "_id":
                    curr_row[key_index] = curr_row[key_index] + all_profiles_dict.get(user_i_likend).get(curr_key)

    all_profiles_i_liked.loc[index] = curr_row

for index, row in all_profiles_i_liked.iterrows():
    for key_index, curr_key in enumerate(columns):
        user_i_likes_counter = all_profiles_i_liked.loc[index].counter

        if user_i_likes_counter == 0:
            continue
        if curr_key != "_id" and curr_key != "counter":
            all_profiles_i_liked.at[index, curr_key] = row[curr_key] / user_i_likes_counter

for index, row in all_profiles_i_liked.iterrows():
    sum = 0
    for key_index, curr_key in enumerate(columns):
        if curr_key != "_id" and curr_key != "counter":
            sum += row[curr_key]

    if sum != 0:
        for key_index, curr_key in enumerate(columns):
            if curr_key != "_id" and curr_key != "counter":
                all_profiles_i_liked.at[index, curr_key] = row[curr_key] / sum

with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                       'apartmentsRecommendationKMeans.pickle'), 'rb') as f:
    kmeans_model = pickle.load(f)

labels_with_profiles_likes_counter = []
profiles_likes_counter_columns = ["profiles", "counter"]

n_clusters = len(kmeans_model.cluster_centers_)

for index in range(n_clusters):
    profiles_likes_counter = pd.DataFrame(columns=profiles_likes_counter_columns)

    for inner_index, curr_email in enumerate(all_profiles.email):
        profiles_likes_counter.loc[inner_index] = [curr_email, 0]

    labels_with_profiles_likes_counter.append(profiles_likes_counter)

ids_with_labels_columns = ["_id", "labels"]
ids_with_labels = pd.DataFrame(columns=ids_with_labels_columns)
ids_with_labels_as_object = []

for index, curr_row in all_profiles_i_liked.iterrows():
    predicted_label = kmeans_model.predict([curr_row.values[2:]])[0]

    ids_with_labels.loc[index] = [curr_row._id, predicted_label]

    ids_with_labels_as_object.append({
        "email": curr_row._id,
        "label": predicted_label.item()
    })

for index, curr_email in all_profiles_choices.iterrows():
    curr_user_label = ids_with_labels[ids_with_labels._id == curr_email._id].labels
    curr_user_profiles_likes_counter = labels_with_profiles_likes_counter[curr_user_label.values[0]]

    for inner_index, curr_profile in enumerate(curr_email.profilesIChose):
        if curr_profile.get("choiceType") == 2 or curr_profile.get("choiceType") == 1:
            profle_i_like = curr_profile.get("profile")
            curr_user_profiles_likes_counter.loc[
                curr_user_profiles_likes_counter.profiles == profle_i_like, "counter"] = \
                curr_user_profiles_likes_counter.loc[
                    curr_user_profiles_likes_counter.profiles == profle_i_like, "counter"] + 1

min_index = 0
min_value = sys.maxsize

for index, curr_vector in enumerate(kmeans_model.cluster_centers_):
    dist = np.linalg.norm(curr_vector - np.zeros(curr_vector.shape[0]))

    if dist < min_value:
        min_value = dist
        min_index = index

labels_with_profiles_likes_counter_as_object = []

for index in range(n_clusters):
    curr_object = {
        "label": index,
        "isCenter": True if index == min_index else False,
        "profiles": []
    }

    for profile_with_counter in labels_with_profiles_likes_counter[index].values.tolist():
        curr_object["profiles"].append({
            "email": profile_with_counter[0],
            "counter": profile_with_counter[1]
        })

    labels_with_profiles_likes_counter_as_object.append(curr_object)

with open(labels_with_counters_results_file_name, 'w', encoding='utf-8') as outfile:
    json.dump(labels_with_profiles_likes_counter_as_object, outfile)

with open(emails_with_labels_results_name, 'w', encoding='utf-8') as outfile:
    json.dump(ids_with_labels_as_object, outfile)

print(labels_with_counters_results_file_name)
print(emails_with_labels_results_name)
