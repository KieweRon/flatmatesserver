import {ChoiceTypeEnum} from "../Enums/ChoiceTypeEnum";

export default interface UserChoice {
    firstUser: string,
    secondUser: string,
    choiceType: ChoiceTypeEnum,
    timestamp?: Date
}