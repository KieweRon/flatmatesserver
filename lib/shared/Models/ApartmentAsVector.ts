import UserModel from "./UserModel";

export default class ApartmentAsVector {
    finalVector: Record<string, number | string>;

    readonly MAX_FLOORS = 50;
    readonly MAX_ROOMMATES_AMOUNT = 10;
    readonly MAX_ROOMS_AMOUNT = 20;
    readonly CITIES = [
        "אום אל-פחם‏",
        "אופקים‏",
        "אור יהודה‏",
        "אור עקיבא‏",
        "אילת‏",
        "אלעד‏",
        "אריאל‏",
        "אשדוד‏",
        "אשקלון",
        "באקה אל-גרבייה‏",
        "באר שבע‏",
        "בית שאן‏",
        "בית שמש‏",
        "ביתר עילית",
        "בני ברק‏",
        "בת ים‏",
        "גבעת שמואל‏",
        "גבעתיים‏",
        "דימונה‏",
        "הוד השרון‏",
        "הרצליה‏",
        "חדרה‏",
        "חולון‏",
        "חיפה‏",
        "טבריה‏",
        "טייבה‏",
        "טירה",
        "טירת כרמל",
        "טמרה‏",
        "יבנה",
        "יהוד-מונוסון‏",
        "יקנעם עילית‏",
        "ירושלים‏",
        "כפר יונה‏",
        "כפר סבא‏",
        "כפר קאסם‏",
        "כרמיאל",
        "לוד‏",
        "מגדל העמק",
        "מודיעין עילית‏",
        "מודיעין-מכבים-רעות‏",
        "מעלה אדומים‏",
        "מעלות-תרשיחא‏",
        "נהריה‏",
        "נס ציונה‏",
        "נצרת‏",
        "נצרת עילית‏",
        "נשר (עיר)",
        "נתיבות",
        "נתניה‏",
        "סח'נין‏",
        "עכו‏",
        "עפולה",
        "עראבה‏",
        "ערד",
        "פתח תקווה‏",
        "צפת‏",
        "קלנסווה‏",
        "קריית אונו‏",
        "קריית אתא‏",
        "קריית ביאליק‏",
        "קריית גת‏",
        "קריית ים‏",
        "קריית מוצקין‏",
        "קריית מלאכי‏",
        "קריית שמונה‏",
        "ראש העין‏",
        "ראשון לציון",
        "רהט‏",
        "רחובות‏",
        "רמלה‏",
        "רמת גן",
        "רמת השרון‏",
        "רעננה‏",
        "שדרות‏",
        "שפרעם",
        "תל אביב-יפו‏"
    ];

    readonly MONTHLY_RENT_RANGES = [
        {
            min: 0,
            max: 1000
        },
        {
            min: 1000,
            max: 2000
        },
        {
            min: 2000,
            max: 3000
        },
        {
            min: 3000,
            max: 4000
        },
        {
            min: 4000,
            max: 5000
        },
        {
            min: 5000,
            max: 6000
        },
        {
            min: 6000,
            max: 7000
        },
        {
            min: 7000,
            max: 8000
        },
        {
            min: 8000,
            max: 9000
        },
        {
            min: 9000,
            max: 10000
        },
        {
            min: 10000,
            max: null
        }
    ];
    readonly SIZE_RANGES = [
        {
            min: 0,
            max: 100
        },
        {
            min: 100,
            max: 200
        },
        {
            min: 200,
            max: 300
        },
        {
            min: 300,
            max: 400
        },
        {
            min: 400,
            max: 500
        },
        {
            min: 500,
            max: 600
        },
        {
            min: 600,
            max: 700
        },
        {
            min: 700,
            max: 800
        },
        {
            min: 800,
            max: 900
        },
        {
            min: 900,
            max: 1000
        },
        {
            min: 1000,
            max: null
        }
    ];

    constructor(usersILiked: Array<Partial<UserModel>>) {
        this.finalVector = {};

        // this._initCityVector();
        this._initMonthlyRentRanges();
        this._initSizeRanges();
        this._initFloors();
        this._initRoommatesAmount();
        this._initRoomsAmount();
        this._initBooleanField("hasElevator");
        this._initBooleanField("hasAirConditioning");
        this._initBooleanField("hasBalcony");
        this._initBooleanField("hasGarden");
        this._initBooleanField("hasParking");
        this._initBooleanField("hasPets");

        this._calcAverageVectorByUsers(usersILiked);
    }

    private _calcAverageVectorByUsers(usersILiked: Array<Partial<UserModel>>) {
        if (usersILiked.length > 0) {

            let valueToAdd = 1 / usersILiked.length;

            usersILiked.forEach((innerUser) => {
                let currProfile = innerUser.apartmentProfile;

                Object.keys(currProfile).forEach((currKey) => {
                    let currKeyInVector;
                    switch (currKey) {
                        // case "city": {
                        //     let currCity = currProfile.city;
                        //     currKeyInVector = `city.${currCity}`;
                        //
                        //     break;
                        // }
                        case "monthlyRent": {
                            currKeyInVector = this.getMonthlyRentRangeByMonthlyRent(currProfile.monthlyRent);

                            break;
                        }
                        case "size": {
                            currKeyInVector = this.getSizeRangeByMonthlyRent(currProfile.size);

                            break;
                        }
                        case "roommatesAmount": {
                            currKeyInVector = this
                                .getRoommatesAmountFieldNameByRoommatesAmount(currProfile.roommatesAmount);
                            break;
                        }
                        case "floor": {
                            currKeyInVector = this.getFloorFieldNameByFloor(currProfile.floor);

                            break;
                        }
                        case "roomAmount": {
                            currKeyInVector = this.getRoomAmountFieldNameByRoomAmount(currProfile.roomAmount);

                            break;
                        }
                        default: {
                            currKeyInVector = `${currKey}.${currProfile[currKey].toString()}`;

                            break;
                        }
                    }

                    this.finalVector[currKeyInVector] = valueToAdd +
                        (this.finalVector[currKeyInVector] as number);
                });
            });
        }
    }

    private _initCityVector() {
        this.CITIES.forEach((currCity) => {
            this.finalVector[`city.${currCity}`] = 0;
        });
    }

    private _initMonthlyRentRanges() {
        this.MONTHLY_RENT_RANGES.forEach((currRentRange) => {
            if (currRentRange.max) {
                this.finalVector[`monthlyRentRanges.${currRentRange.max}-${currRentRange.min}`] = 0;
            } else {
                this.finalVector[`monthlyRentRanges.${currRentRange.min}+`] = 0;
            }
        });
    }

    private _initSizeRanges() {
        this.SIZE_RANGES.forEach((currSizeRange) => {
            if (currSizeRange.max) {
                this.finalVector[`size.${currSizeRange.max}-${currSizeRange.min}`] = 0;
            } else {
                this.finalVector[`size.${currSizeRange.min}+`] = 0;
            }
        });
    }

    private _initFloors() {
        for (let i = 0; i < this.MAX_FLOORS; i++) {
            this.finalVector[`floor.${i}`] = 0;
        }

        this.finalVector[`floor.${this.MAX_FLOORS}+`] = 0;
    }

    private _initRoommatesAmount() {
        for (let i = 0; i < this.MAX_ROOMMATES_AMOUNT; i++) {
            this.finalVector[`roommatesAmount.${i}`] = 0;
        }

        this.finalVector[`roommatesAmount.${this.MAX_ROOMMATES_AMOUNT}+`] = 0;
    }

    private _initRoomsAmount() {
        for (let i = 0; i < this.MAX_ROOMS_AMOUNT; i++) {
            this.finalVector[`roomAmount.${i}`] = 0;
        }

        this.finalVector[`roomAmount.${this.MAX_ROOMS_AMOUNT}+`] = 0;
    }

    private _initBooleanField(fieldName: string) {
        this.finalVector[`${fieldName}.true`] = 0;
        this.finalVector[`${fieldName}.false`] = 0;
    }

    public getMonthlyRentRangeByMonthlyRent(monthlyRent: number): string {
        let index = this.MONTHLY_RENT_RANGES.findIndex((currRange) => {
            if ((monthlyRent > currRange.min) && (monthlyRent < currRange.max)) {
                return true;
            }
        });

        if (index !== -1) {
            return `monthlyRentRanges.${this.MONTHLY_RENT_RANGES[index].max}-${this.MONTHLY_RENT_RANGES[index].min}`;
        } else {
            return `monthlyRentRanges.${this.MONTHLY_RENT_RANGES[this.MONTHLY_RENT_RANGES.length - 1].min}+`;
        }
    }

    public getSizeRangeByMonthlyRent(monthlyRent: number): string {
        let index = this.SIZE_RANGES.findIndex((currRange) => {
            if ((monthlyRent > currRange.min) && (monthlyRent < currRange.max)) {
                return true;
            }
        });

        if (index !== -1) {
            return `size.${this.SIZE_RANGES[index].max}-${this.SIZE_RANGES[index].min}`;
        } else {
            return `size.${this.SIZE_RANGES[this.SIZE_RANGES.length - 1].min}+`;
        }
    }

    public getRoommatesAmountFieldNameByRoommatesAmount(roommatesAmount: number): string {
        if (this.MAX_ROOMMATES_AMOUNT > roommatesAmount) {
            return `roommatesAmount.${roommatesAmount}`;
        } else {
            return `roommatesAmount.${this.MAX_ROOMMATES_AMOUNT}+`;
        }
    }

    public getFloorFieldNameByFloor(floor: number): string {
        if (this.MAX_FLOORS > floor) {
            return `floor.${floor}`;
        } else {
            return `floor.${this.MAX_FLOORS}+`;
        }
    }

    public getRoomAmountFieldNameByRoomAmount(roomAmount: number): string {
        if (this.MAX_ROOMS_AMOUNT > roomAmount) {
            return `roomAmount.${roomAmount}`;
        } else {
            return `roomAmount.${this.MAX_ROOMS_AMOUNT}+`;
        }
    }
}