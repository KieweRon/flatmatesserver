import {EmailWithCounter} from "./EmailWithCounter";

export default interface AlgoRecommendationResults {
    label: number;
    isCenter: boolean;
    profiles: Array<EmailWithCounter>;
}