export default interface AlgoLabelingResults {
    label: number;
    email: string;
}