import Roommate from "./RoommateModel";
import Apartment from "./ApartmentModel";
import {UserPreferences} from "../types/UserPreferencesType";
import {UserTypeEnum} from "../Enums/UserTypeEnum";

export default interface User {
    type: UserTypeEnum,
    password: string,
    email: string,
    phoneNumber: string,
    isActive: boolean,
    isDeleted: boolean,
    apartmentProfile: Apartment,
    roommateProfile: Roommate,
    userPreferences: UserPreferences
}