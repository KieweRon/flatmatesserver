import UserModel from "./UserModel";
import {SexEnum} from "../Enums/SexEnum";
import {ReligionHabitsEnum} from "../Enums/ReligionHabitsEnum";
import {EatingHabitsEnum} from "../Enums/EatingHabitsEnum";

export default class RoommatesAsVector {
    finalVector: Record<string, number | string>;

    readonly MAX_AGE = 39;
    readonly MIN_AGE = 18;

    constructor(usersILiked: Array<Partial<UserModel>>) {
        this.finalVector = {};

        this._initAges();
        this._initSexs();
        this._initReligionHabits();
        this._initEatingHabits();
        this._initBooleanField("hasPets");
        this._initBooleanField("isSmoke");
        this._initBooleanField("isSmokeWeed");

        this._calcAverageVectorByUsers(usersILiked);
    }

    private _calcAverageVectorByUsers(usersILiked: Array<Partial<UserModel>>) {
        let valueToAdd = 1 / usersILiked.length;

        usersILiked.forEach((innerUser) => {
            let currProfile = innerUser.roommateProfile;

            Object.keys(currProfile).forEach((currKey) => {
                let currKeyInVector;
                switch (currKey) {
                    case "religionHabits": {
                        currProfile.religionHabits.forEach((currReligionHabit) => {
                            currKeyInVector = `religionHabits.${ReligionHabitsEnum[currReligionHabit]}`;

                            this.finalVector[currKeyInVector] = valueToAdd +
                                (this.finalVector[currKeyInVector] as number);
                        });

                        break;
                    }
                    case "eatingHabits": {
                        currProfile.eatingHabits.forEach((currEatingHabit) => {
                            currKeyInVector = `eatingHabits.${EatingHabitsEnum[currEatingHabit]}`;

                            this.finalVector[currKeyInVector] = valueToAdd +
                                (this.finalVector[currKeyInVector] as number);
                        });

                        break;
                    }
                    case "age": {
                        currKeyInVector = this.getRoomAmountFieldNameByRoomAmount(currProfile.age);

                        this.finalVector[currKeyInVector] = valueToAdd +
                            (this.finalVector[currKeyInVector] as number);
                        break;
                    }
                    default: {
                        currKeyInVector = `${currKey}.${currProfile[currKey].toString()}`;

                        this.finalVector[currKeyInVector] = valueToAdd +
                            (this.finalVector[currKeyInVector] as number);

                        break;
                    }
                }
            });
        });
    }

    private _initSexs() {
        Object.keys(SexEnum).forEach((currSex) => {
            this.finalVector[`sex.${SexEnum[currSex]}`] = 0;
        });
    }

    private _initReligionHabits() {
        Object.keys(ReligionHabitsEnum).forEach((currReligionHabit) => {
            this.finalVector[`religionHabits.${ReligionHabitsEnum[currReligionHabit]}`] = 0;
        });
    }

    private _initEatingHabits() {
        Object.keys(EatingHabitsEnum).forEach((currEatingHabit) => {
            this.finalVector[`eatingHabits.${EatingHabitsEnum[currEatingHabit]}`] = 0;
        });
    }

    private _initAges() {
        for (let i = this.MIN_AGE; i < this.MAX_AGE; i++) {
            this.finalVector[`age.${i}`] = 0;
        }

        this.finalVector[`age.${this.MAX_AGE}+`] = 0;
    }

    private _initBooleanField(fieldName: string) {
        this.finalVector[`${fieldName}.true`] = 0;
        this.finalVector[`${fieldName}.false`] = 0;
    }

    public getRoomAmountFieldNameByRoomAmount(age: number): string {
        if (this.MAX_AGE > age) {
            return `age.${age}`;
        } else {
            return `age.${this.MAX_AGE}+`;
        }
    }
}