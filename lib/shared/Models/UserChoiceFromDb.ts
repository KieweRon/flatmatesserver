import {ChoiceTypeEnum} from "../Enums/ChoiceTypeEnum";
import User from "./UserModel";

export default interface UserChoiceFromDb {
    firstUser: string;
    secondUser: string;
    choiceType: ChoiceTypeEnum;
    timestamp?: Date;
    likedUser: Array<User>
}