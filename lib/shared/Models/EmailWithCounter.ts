export interface EmailWithCounter {
    email: string;
    counter: number
}