import {ChoiceTypeEnum} from "../Enums/ChoiceTypeEnum";

export interface UserChoiceGroupedByEmail {
    _id: string;
    profilesIChose: Array<{
        profile: string;
        choiceType: ChoiceTypeEnum
    }>
}