import {ReligionHabitsEnum} from "../Enums/ReligionHabitsEnum";
import {EatingHabitsEnum} from "../Enums/EatingHabitsEnum";
import {HobbiesEnum} from "../Enums/HobbiesEnum";
import {SexEnum} from "../Enums/SexEnum";

export default interface Roommate {
    name: string;
    age: number;
    sex: SexEnum;
    date: number;
    picture: string;
    hasPets: boolean;
    isSmoke: boolean;
    isSmokeWeed: boolean;
    religionHabits: ReligionHabitsEnum[];
    eatingHabits: EatingHabitsEnum[];
    hobbies: HobbiesEnum[];
    comments: string;
}