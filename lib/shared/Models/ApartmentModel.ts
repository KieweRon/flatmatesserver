import Roommate from "./RoommateModel";

export default interface Apartment {
    address: string;
    size: number;
    city: string;
    floor: number;
    picture: string;
    monthlyRent: number;
    roomAmount: number;
    hasElevator: boolean;
    hasAirConditioning: boolean;
    hasBalcony: boolean;
    hasGarden: boolean;
    hasParking: boolean;
    hasPets: boolean;
    comments: string;
    roommatesAmount: number;
    roommates: Roommate[];
}