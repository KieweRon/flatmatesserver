export enum ApartmentPreferencesEnum {
    MaxMonthlyRent = 1,
    Cities = 2,
    RoomsNumber = 3,
    ElevatorOnly = 4,
    MaxFloor = 5,
    AirConditionOnly = 6,
    WithBalcony = 7,
    WithParking = 8,
    MinSize = 9,
    MaxSize = 10,
    MaxRoommatesNumber = 11,
    MinRoommatesNumber = 12
}