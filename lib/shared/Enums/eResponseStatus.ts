export enum eResponseStatus {
    Success = 1,
    InternalError = 2,
    DuplicateUser = 3
}