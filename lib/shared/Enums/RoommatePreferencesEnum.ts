export enum RoommatePreferencesEnum {
    MinAge = 1,
    MaxAge = 2,
    IsSmoking = 3,
    IsWeedFriendly = 4,
    Sex = 5,    
    HasPets = 6,
    EatKosher = 7
}