export enum ReligionHabitsEnum {
    SABATH_KEEPING = "שומר שבת",
    SECULAR = "חילוני",
    ORTHODOX = "מסורתי"
}
