export enum ChoiceTypeEnum{
    LIKE_ROOMMATE = 1, //apartment likes roommate
    LIKE_APARTMENT = 2, //roommate likes apartment
    DISLIKE_ROOMMATE = 3, // apartment dislikes roommate
    DISLIKE_APARTMENT = 4 //roommate dislikes apartment
}