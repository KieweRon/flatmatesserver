export enum UserTypeEnum {
    ROOMMATE_SEARCH = "RoommateSearch",
    APARTMENT_SEARCH = "ApartmentSearch"
}
