export enum EatingHabitsEnum {
    KOSHER = "כשר",
    KOSHER_LE_MEHADRIN = "כשר למהדרין",
    VEGETERIAN = "צמחוני",
    VEGAN = "טבעוני",
    GLUTENF_REE = "ללא גלוטן",
    NUTS_FREE = "ללא אגוזים"
}