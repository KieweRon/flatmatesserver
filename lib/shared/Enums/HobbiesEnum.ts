export enum HobbiesEnum {
    COOKING = "בישול",
    HIKING = "טיפוס",
    SURFING = "גלישה",
    NETFLIX = "נטפליקס",
    PHOTOGRAPHY = "צילום",
    NATURE_TRIPS = "טיולי טבע"
}
